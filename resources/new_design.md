# New design of the language

The current version of the language is very simple, basicaly just barely an excuse to build a compiler. This new version is a total redesign, should have abstruction power higher than C.

We will slowly rewrite the compiler to match this new language.

This design document is not final and will change during the rewriting of the compiler.

# Inbuilt Types
- ## Unsigned integers
    - ```u8, u16, u32, u64, u128```

- ## Signed integers
    - ```i8, i16, i32, i64, i128```

- ## Floats
    - Operations are asociative even if the they change the result's binary representation
    - Does not have special values like +/- inf, nan, ...
    - ```f16,  f32,  f64,  f128```

- ## Hardware floats
    - Floating point numbers as exactly implemented in the hardware of current architecture
    - Operations are not asociative if they change the result's binary representation
    - ```hf16, hf32, hf64, hf128```

- ## Boolean
    - Same size as the smallest addressable word of the current architecture
    - ```bool```

- ## PtrSize
    - Alias to an unsigned or signed integer type of the same size as pointer types of the current architecture
    - ```usize```, ```isize```

- ## Pointers to regions with compile-time known size
    - Pointers have to point to a sized region of memory. The size of the region is part of the pointer's type.
    - You can have a pointer to a zero sized region, just cannot dereference it
    - ```u8[1]*```, ```bool[1]*```, ```u64[512]*```

- ## Functions
    - TODO: Works as you expect I guess. Same like C or C++ or Rust. I don't think there are big differences in their basic workings.
    - ```fn foo(u64 arg) -> u64```

- ## Structs
    - TODO

- ## Classes
    - TODO: Are they different from structs? Do we want structs *and* classes?
    - TODO: Do we have explicit or implicit constructors and destructors? Do we have them at all?

# Standard library types
- ## Pointers to regions with runtime known size
    - A pair of values: The actual address the pointer points to, and a size of the region
    - The whole runtime sized region needs to be inside another region (eighter runtime sized or compile-time sized region).
    - TODO: Can the constructor do a runtime check to see if the new inner region is fully within the outer region? How would it return an error? Maybe a factory function that returns Result<...> or something...
    ```
    u64[512] array = [0; 512];
    usize range_offset = 64;
    usize range_len = 32;

    RuntimeSizedPtr<u64[512]> ptr(array, range_offset, range_len);
    ```

# Type modifiers
- TODO: volatile, aligned_to/unaligned, calling convention, ...

# Type templates / generics
- TODO

# Variable modifiers
- TODO: mut/const, ...
