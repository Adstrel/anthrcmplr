INPUT_CPP := \
	src/main.cpp

INPUT_HPP := \
	src/Common.hpp \
	src/Arguments.hpp \
	src/Magic.hpp \
	src/Compiler.hpp \
	src/Compiler/Token.hpp \
	src/Compiler/Lexer.hpp \
	src/Compiler/TokenListPrinter.hpp \
	src/Compiler/Parser.hpp \
	src/Compiler/AST/ExecType.hpp \
	src/Compiler/AST/Expression.hpp \
	src/Compiler/AST/Scope.hpp \
	src/Compiler/AST/Statement.hpp \
	src/Compiler/AST/AstVisitor.hpp \
	src/Compiler/AST/Visitors/AstPrinter.hpp \
	src/Compiler/AST/Visitors/DotGener.hpp \
	src/Compiler/AST/Visitors/CppCodegener.hpp \
	src/Compiler/AST/Visitors/TypeResolver.hpp \
	src/Compiler/AST/Visitors/IRBuilder.hpp \
	src/Compiler/IR/Program.hpp \
	src/Compiler/IR/Function.hpp \
	src/Compiler/IR/CodeBlock.hpp \
	src/Compiler/IR/Assignment.hpp

CXX := g++-11
FLAGS := -Wall -Wextra -pedantic -std=c++23 -O3 -flto -march=x86-64-v3 -Isrc -g -static

.PHONY: clean

comp: Makefile $(INPUT_CPP) $(INPUT_HPP)
	$(CXX) $(INPUT_CPP) -o $@ $(FLAGS)

clean:
	rm -f comp out.cpp a.out out.dot out.png

