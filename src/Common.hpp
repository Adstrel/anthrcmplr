#pragma once
#include <fstream>
#include <sstream>
#include <cstdio>


#define UNUSED(x) (void) (x)

struct ExitMsg {
    int exit_code;

    ExitMsg(int exit_code)
        : exit_code(exit_code) {}
};

// Let's use this until we get std::format implemented in gcc and clang
template<typename... Args_t>
inline std::string formatf(const char* fmt, Args_t... args) {
    int ret = snprintf(nullptr, 0, fmt, args...);
    if (ret < 0) {
        fprintf(stderr, "Internal Compiler Error: Cannot format a message!\n");
        throw ExitMsg(1);
    }

    size_t len = ret + 1;
    std::string result(len, '\0');
    snprintf(result.data(), len, fmt, args...);
    return result;
}

inline std::string read_from_file(const std::string& filename) {
    std::ifstream fin(filename);

    if (!fin.is_open()) {
        fprintf(stderr, "Cannot open input file \"%s\"!\n", filename.c_str());
        throw ExitMsg(1);
    }

    std::stringstream buffer;
    buffer << fin.rdbuf();
    return buffer.str();
}

inline void write_to_file(const std::string& filename, const std::string& data) {
    std::ofstream fout(filename);

    if (!fout.is_open()) {
        fprintf(stderr, "Cannot open file \"%s\" for writing!\n", filename.c_str());
        throw ExitMsg(1);
    }

    fout << data;
}

