#pragma once
#include "Compiler/IR/Function.hpp"


namespace Compiler::IR {

struct Program {
    std::map<std::string, Function> func_dict;
};

} // namespace Compiler::IR
