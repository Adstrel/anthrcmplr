#pragma once
#include "Compiler/IR/CodeBlock.hpp"

#include <map>


namespace Compiler::IR {

struct Function {
    std::string name;
    DataType return_type;
    std::vector<std::pair<
        DataType, Assignment::VarIdent
    >> arguments;

    std::map<Assignment::VarIdent, std::shared_ptr<Assignment>> var_dict;
    std::shared_ptr<CodeBlock> entry_point;
};

} // namespace Compiler::IR
