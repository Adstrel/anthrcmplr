#pragma once
#include <tuple>
#include <string>
#include <vector>
#include <variant>


namespace Compiler::IR {

enum class DataType {
    i8, i16, i32, i64, i128,
    u8, u16, u32, u64, u128,
    Bool,
    ptr
};

struct Assignment;
struct CodeBlock;
struct Function;

struct Assignment {
    // Unique identificator of a variable in the SSA form
    using VarIdent = std::pair<std::string, uint64_t>;

    struct Simple {
        VarIdent assigned_variable;
    };

    struct UnOp {
        enum class Op {
            Minus,
            BitwiseNot,
            BoolNot
        } op;
        VarIdent var;
    };

    struct BinOp {
        enum class Op {
            Equals,
            NotEquals,
            Less,
            LessEquals,
            More,
            MoreEquals,
            Plus,
            Minus,
            Times,
            Divide,
            Modulo,
            BitwiseAnd,
            BitwiseOr,
            BitwiseXor,
            BoolAnd,
            BoolOr
        } op;
        VarIdent left;
        VarIdent right;
    };

    struct FuncCall {
        std::string func_name;
        std::vector<VarIdent> arguments;
    };

    struct TypeCast {
        VarIdent var;
    };

    struct AddrOf {
        VarIdent var;
    };

    struct PtrDeref {
        VarIdent var_ptr;
    };

    struct PtrArithmetic {
        enum class Op {
            PtrPlusInt,
            PtrMinusInt,
            PtrTimesInt,
            PtrDivideInt,
            PtrMinusPtr,
            PtrModPtr
        } op;
        VarIdent left;
        VarIdent right;
    };

    struct OneOf {
        std::vector<VarIdent> posibilities;
    };

    DataType data_type;
    VarIdent new_identificator;
    std::variant<
        Simple,
        UnOp,
        BinOp,
        FuncCall,
        TypeCast,
        AddrOf,
        PtrDeref,
        PtrArithmetic,
        OneOf
    > assignment_data;
};

} // namespace Compiler::IR
