#pragma once
#include "Compiler/IR/Assignment.hpp"

#include <memory>


namespace Compiler::IR {

struct CodeBlock {
    struct IfElse {
        Assignment::VarIdent condition_var;
        std::shared_ptr<CodeBlock> if_path;
        std::shared_ptr<CodeBlock> else_path;
    };

    struct Jump {
        std::shared_ptr<CodeBlock> next_block;
    };

    struct Return {
        Assignment::VarIdent ret_var;
    };

    std::vector<Assignment::VarIdent> code_lines;
    std::variant<IfElse, Jump, Return> block_end;
};

} // Compiler::IR
