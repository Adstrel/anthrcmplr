#pragma once
#include <variant>
#include <memory>
#include <vector>
#include <cassert>


namespace Compiler::AST {

struct ExecType {
    enum class BasicType {
        Unknown = 0,
        Void,
        String,
        Number,
        Bool
    };

    struct FunctionType {
        FunctionType(const FunctionType& other)
            : return_type(other.return_type ? std::make_unique<ExecType>(*other.return_type) : nullptr), arg_list(other.arg_list), name(other.name) {}

        FunctionType(FunctionType&& other) = default;

        FunctionType(std::unique_ptr<ExecType>&& return_type, std::vector<std::pair<ExecType, std::string>>&& arg_list, std::string name)
            : return_type(std::move(return_type)), arg_list(std::move(arg_list)), name(std::move(name)) {}

        FunctionType& operator=(const FunctionType& other) {
            return_type = other.return_type ? std::make_unique<ExecType>(*other.return_type) : nullptr;
            arg_list = other.arg_list;
            name = other.name;
            return *this;
        }

        bool operator==(const FunctionType& other) const {
            assert(return_type);
            assert(other.return_type);

            return *return_type == *other.return_type &&
                std::ranges::equal(
                    arg_list, other.arg_list,
                    [] (const auto& l, const auto& r) -> bool { return l.first == r.first; }
                );
        }

        std::unique_ptr<ExecType> return_type;
        std::vector<std::pair<ExecType, std::string>> arg_list;
        std::string name;
    };

    struct CustomType {
        std::vector<ExecType> member_list;

        bool operator==(const CustomType& other) const { return member_list == other.member_list; }
    };

    ExecType(BasicType actual_type = BasicType::Unknown, bool is_mutable = false)
        : is_mutable(is_mutable), actual_type(actual_type) {}

    ExecType(FunctionType actual_type, bool is_mutable = false)
        : is_mutable(is_mutable), actual_type(std::move(actual_type)) {}

    ExecType(CustomType actual_type, bool is_mutable = false)
        : is_mutable(is_mutable), actual_type(std::move(actual_type)) {}

    bool operator==(const ExecType& other) const {
        return actual_type == other.actual_type;
    }

    bool is_mutable = false;

    // Index -1 - the type is currently unknown and will be computed in a later pass after the AST is built
    // Index  0 - A basic inbuilt type
    // Index  1 - A function type
    // Index  2 - A custom type (struct/class/whatever)
    std::variant<BasicType, FunctionType, CustomType> actual_type;
};

} // namespace Compiler::NewAST
