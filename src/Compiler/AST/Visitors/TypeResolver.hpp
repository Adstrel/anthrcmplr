#pragma once
#include "Compiler/AST/Scope.hpp"


namespace Compiler::AST {

class TypeResolver : public AstVisitor {
    struct FunctionCode {
        StatementDeclFn* func;
        std::vector<Scope*> scopes;

        FunctionCode(StatementDeclFn* func)
            : func(func) {}
    };

    std::vector<FunctionCode> stack_of_functions;
    Scope& global_scope;

    TypeResolver(Scope& global_scope)
        : global_scope(global_scope) {}

    virtual void visit_statement(Statement&) {
        assert(false);
    }

    virtual void visit_statement(StatementEmpty&) {}

    virtual void visit_statement(StatementExpression& stmnt) {
        stmnt.expr->accept(*this);
    }

    virtual void visit_statement(StatementPrint& stmnt) {
        stmnt.expr->accept(*this);

        if (stmnt.expr->exec_type == ExecType{ExecType::BasicType::Void}) {
            fprintf(stderr, "Error: On line %ld, position %ld, trying to print a value of type Void!\n",
                stmnt.expr->line_num,
                stmnt.expr->line_pos
            );
            throw ExitMsg(1);
        }
    }

    virtual void visit_statement(StatementBlock& stmnt) {
        stack_of_functions.back().scopes.push_back(stmnt.local_scope.get());
        for (std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            st->accept(*this);
        }
        stack_of_functions.back().scopes.pop_back();
    }

    virtual void visit_statement(StatementIfElse& stmnt) {
        stmnt.condition->accept(*this);

        if (stmnt.condition->exec_type != ExecType{ExecType::BasicType::Bool}) {
            fprintf(stderr, "Error: On line %ld, position %ld, condition of an if statement is not of type Bool!\n",
                stmnt.condition->line_num,
                stmnt.condition->line_pos
            );
            throw ExitMsg(1);
        }

        stmnt.if_branch->accept(*this);
        if (stmnt.else_branch) {
            stmnt.else_branch->accept(*this);
        }
    }

    virtual void visit_statement(StatementWhile& stmnt) {
        stmnt.condition->accept(*this);

        if (stmnt.condition->exec_type != ExecType{ExecType::BasicType::Bool}) {
            fprintf(stderr, "Error: On line %ld, position %ld, condition of a while statement is not of type Bool!\n",
                stmnt.condition->line_num,
                stmnt.condition->line_pos
            );
            throw ExitMsg(1);
        }

        stmnt.body->accept(*this);
    }

    virtual void visit_statement(StatementFor& stmnt) {
        stack_of_functions.back().scopes.push_back(stmnt.stmnt1_scope.get());
        for (std::unique_ptr<Statement>& st : stmnt.stmnt1_scope->body) {
            st->accept(*this);
        }

        if (stmnt.expr2) {
            stmnt.expr2->accept(*this);
        }

        if (stmnt.expr2 && stmnt.expr2->exec_type != ExecType{ExecType::BasicType::Bool}) {
            fprintf(stderr, "Error: On line %ld, position %ld, condition of a for statement is not of type Bool!\n",
                stmnt.expr2->line_num,
                stmnt.expr2->line_pos
            );
            throw ExitMsg(1);
        }

        if (stmnt.expr3) {
            stmnt.expr3->accept(*this);
        }

        stmnt.body->accept(*this);
        stack_of_functions.back().scopes.pop_back();
    }

    virtual void visit_statement(StatementReturn& stmnt) {
        stmnt.expr->accept(*this);

        const std::unique_ptr<ExecType>& return_type = std::get<ExecType::FunctionType>(stack_of_functions.back().func->exec_type.actual_type).return_type;

        if ((!!stmnt.expr != !!return_type) || (stmnt.expr && return_type &&
                stmnt.expr->exec_type != *std::get<ExecType::FunctionType>(stack_of_functions.back().func->exec_type.actual_type).return_type)) {
            fprintf(stderr, "Error: \n");
            throw ExitMsg(1);
        }
    }

    virtual void visit_statement(StatementDecl&) {
        assert(false);
    }

    virtual void visit_statement(StatementDeclVar& stmnt) {
        if (stmnt.assign_expr) {
            stmnt.assign_expr->accept(*this);
        }
    }

    virtual void visit_statement(StatementDeclFn& stmnt) {
        for (const StatementDecl& arg : stmnt.arguments) {
            if (arg.exec_type == ExecType{ExecType::BasicType::Void}) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to use a value of type Void as a function argument!\n",
                    arg.line_num,
                    arg.line_pos
                );
                throw ExitMsg(1);
            }
        }

        stack_of_functions.emplace_back(&stmnt);
        stack_of_functions.back().scopes.push_back(stmnt.local_scope.get());
        for (std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            st->accept(*this);
        }
        stack_of_functions.pop_back();
    }


    virtual void visit_expression(Expression&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionUnary& expr) {
        expr.expr->accept(*this);

        if (expr.expr->exec_type == ExecType{ExecType::BasicType::Void}) {
            fprintf(stderr, "Error: On line %ld, position %ld, trying to use a value of type Void in a unary expression!\n",
                expr.line_num,
                expr.line_pos
            );
            throw ExitMsg(1);
        }

        switch (expr.op) {
        case ExpressionUnary::Op::Minus:
            if (expr.expr->exec_type != ExecType{ExecType::BasicType::Number}) {
                fprintf(stderr, "Error: Unary operator 'Minus' used with type other than Number ('TODO') on line %ld, position %ld!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = ExecType{ExecType::BasicType::Number};
            break;
        case ExpressionUnary::Op::Not:
            if (expr.expr->exec_type != ExecType{ExecType::BasicType::Bool}) {
                fprintf(stderr, "Error: Unary operator 'Not' used with type other than Bool ('TODO') on line %ld, position %ld!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = ExecType{ExecType::BasicType::Bool};
            break;
        default:
            assert(false);
        }
    }

    virtual void visit_expression(ExpressionBinary& expr) {
        expr.expr_l->accept(*this);
        expr.expr_r->accept(*this);

        if (expr.expr_l->exec_type == ExecType{ExecType::BasicType::Void} || expr.expr_r->exec_type == ExecType{ExecType::BasicType::Void}) {
            fprintf(stderr, "Error: On line %ld, position %ld, trying to use a value of type Void in a binary expression!\n",
                expr.line_num,
                expr.line_pos
            );
            throw ExitMsg(1);
        }

        switch (expr.op) {
        case ExpressionBinary::Op::Minus:
        case ExpressionBinary::Op::Plus:
        case ExpressionBinary::Op::Times:
        case ExpressionBinary::Op::Divide:
            if (expr.expr_l->exec_type != ExecType{ExecType::BasicType::Number} || expr.expr_r->exec_type != ExecType{ExecType::BasicType::Number}) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to use a binary operator with wrong types!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = ExecType{ExecType::BasicType::Number};
            break;
        case ExpressionBinary::Op::Or:
        case ExpressionBinary::Op::And:
            if (expr.expr_l->exec_type != ExecType{ExecType::BasicType::Bool} || expr.expr_r->exec_type != ExecType{ExecType::BasicType::Bool}) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to use a binary operator with wrong types!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = ExecType{ExecType::BasicType::Bool};
            break;
        case ExpressionBinary::Op::Assign:
            if (expr.expr_l->exec_type != expr.expr_r->exec_type) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to assign a value of a different type!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            if (!expr.expr_l->exec_type.is_mutable) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to assign to an unmutable value!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = expr.expr_l->exec_type;
            break;
        case ExpressionBinary::Op::Equals:
        case ExpressionBinary::Op::NotEquals:
            if (expr.expr_l->exec_type != expr.expr_r->exec_type) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to compare values of two different types!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            if (expr.expr_l->exec_type.actual_type.index() != 0) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to compare values of non-basic types!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = ExecType{ExecType::BasicType::Bool};
            break;
        case ExpressionBinary::Op::LessThan:
        case ExpressionBinary::Op::MoreThan:
        case ExpressionBinary::Op::LessThanOrEquals:
        case ExpressionBinary::Op::MoreThanOrEquals:
            if (expr.expr_l->exec_type != expr.expr_r->exec_type) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to compare values of two different types!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            if (expr.expr_r->exec_type != ExecType{ExecType::BasicType::Number}) {
                fprintf(stderr, "Error: On line %ld, position %ld, trying to compare values of non-basic types!\n",
                    expr.line_num,
                    expr.line_pos
                );
                throw ExitMsg(1);
            }
            expr.exec_type = ExecType{ExecType::BasicType::Bool};
            break;
        default:
            assert(false);
        }
    }

    virtual void visit_expression(ExpressionCall& expr) {
        expr.callable->accept(*this);

        if (expr.callable->exec_type.actual_type.index() != 1) {
            fprintf(stderr, "Error: On line %ld, position %ld, trying to call a value that doesn't evaluate to a function!\n",
                expr.callable->line_num,
                expr.callable->line_pos
            );
            throw ExitMsg(1);
        }

        for (std::unique_ptr<Expression>& arg : expr.arg_list) {
            arg->accept(*this);
        }

        const auto& called_func = std::get<ExecType::FunctionType>(expr.callable->exec_type.actual_type);

        if (expr.arg_list.size() != called_func.arg_list.size()) {
            fprintf(stderr, "Error: On line %ld, position %ld, function call to function '%s' has %ld arguments, but %ld was expected!\n",
                expr.line_num,
                expr.line_pos,
                called_func.name.c_str(),
                expr.arg_list.size(),
                called_func.arg_list.size()
            );
            throw ExitMsg(1);
        }

        for (size_t i = 0; i < expr.arg_list.size(); i++) {
            Expression& arg = *expr.arg_list[i];
            arg.accept(*this);
            const auto& [func_arg_type, func_arg_name] = std::get<ExecType::FunctionType>(expr.callable->exec_type.actual_type).arg_list[i];
            if (arg.exec_type != func_arg_type) {
                fprintf(stderr, "Error: On line %ld, position %ld, argument '%s' of function call to '%s' has type '%s', but '%s' was expected!\n",
                    arg.line_num,
                    arg.line_pos,
                    func_arg_name.c_str(),
                    called_func.name.c_str(),
                    "TODO", "TODO"
                );
                throw ExitMsg(1);
            }
        }

        expr.exec_type = *std::get<1>(expr.callable->exec_type.actual_type).return_type;
    }

    virtual void visit_expression(ExpressionGroup& expr) {
        expr.expr->accept(*this);
        expr.exec_type = expr.expr->exec_type;
    }

    virtual void visit_expression(ExpressionLiteral&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionLiteralString& expr) {
        assert(expr.exec_type == ExecType(ExecType::BasicType::String));
        UNUSED(expr);
    }

    virtual void visit_expression(ExpressionLiteralNumber& expr) {
        assert(expr.exec_type == ExecType(ExecType::BasicType::Number));
        UNUSED(expr);
    }

    virtual void visit_expression(ExpressionLiteralBool& expr) {
        assert(expr.exec_type == ExecType(ExecType::BasicType::Bool));
        UNUSED(expr);
    }

    virtual void visit_expression(ExpressionLiteralIdentifier& expr) {
        for (auto it_local_scope = stack_of_functions.back().scopes.rbegin(); it_local_scope != stack_of_functions.back().scopes.rend(); ++it_local_scope) {
            if (auto it = (*it_local_scope)->bindings.find(expr.name); it != (*it_local_scope)->bindings.end()) {
                expr.exec_type = it->second.exec_type;
                return;
            }
        }

        if (auto it = global_scope.bindings.find(expr.name); it != global_scope.bindings.end()) {
            expr.exec_type = it->second.exec_type;
            return;
        }

        fprintf(stderr, "Error: Undeclared identifier '%s' on line %ld, position %ld!\n",
            expr.name.c_str(),
            expr.line_num,
            expr.line_pos
        );
        throw ExitMsg(1);
    }


    void resolve() {
        stack_of_functions.emplace_back(nullptr);
        stack_of_functions.back().scopes.push_back(&global_scope);
        for (std::unique_ptr<Statement>& st : global_scope.body) {
            st->accept(*this);
        }
    }

public:
    static void resolve_types(Scope& global_scope) {
        TypeResolver resolver(global_scope);
        resolver.resolve();
    }
};

} // namespace Compiler::AST
