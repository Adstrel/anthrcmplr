#pragma once
#include "Compiler/AST/Scope.hpp"


namespace Compiler::AST {

class CppCodegener : public AstVisitor {
    const Scope& global_scope;
    std::string output;
    uint32_t indent = 0;
    bool skip_indent = false;

    CppCodegener(const Scope& global_scope)
        : global_scope(global_scope) {}

    void generate_indent() {
        if (skip_indent) {
            return;
        }
        for (uint32_t i = 0; i < indent; i++) {
            output += "    ";
        }
    }

    void generate_type(const ExecType& exec_type, bool print_mut = true) {
        if (print_mut && !exec_type.is_mutable) {
            output += "const ";
        }

        switch (exec_type.actual_type.index()) {
        case 0:
            switch (std::get<0>(exec_type.actual_type)) {
            case ExecType::BasicType::String:
                output += "std::string_view";
                break;
            case ExecType::BasicType::Number:
                output += "int";
                break;
            case ExecType::BasicType::Bool:
                output += "bool";
                break;
            case ExecType::BasicType::Void:
                output += "void";
                break;
            default:
                assert(false);
            }
            break;
        case 1:
        case 2:
        case std::variant_npos:
        default:
            assert(false);
        }
    }

    virtual void visit_statement(Statement&) {
        assert(false);
    }

    virtual void visit_statement(StatementEmpty&) {
        generate_indent();
        output += ";\n";
    }

    virtual void visit_statement(StatementExpression& stmnt) {
        generate_indent();
        stmnt.expr->accept(*this);
        output += ";";
        if (!skip_indent) {
            output += "\n";
        }
    }

    virtual void visit_statement(StatementPrint& stmnt) {
        generate_indent();
        output += "std::cout << (";
        stmnt.expr->accept(*this);
        output += ") << '\\n';\n";
    }

    virtual void visit_statement(StatementBlock& stmnt) {
        generate_indent();
        output += "{\n";
        ++indent;
        for (const std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            st->accept(*this);
        }
        --indent;
        generate_indent();
        output += "}\n";
    }

    virtual void visit_statement(StatementIfElse& stmnt) {
        generate_indent();
        output += "if (";
        stmnt.condition->accept(*this);
        output += ")\n";
        ++indent;
        stmnt.if_branch->accept(*this);
        --indent;
        if (stmnt.else_branch) {
            generate_indent();
            output += "else\n";
            ++indent;
            stmnt.else_branch->accept(*this);
            --indent;
        }
    }

    virtual void visit_statement(StatementWhile& stmnt) {
        generate_indent();
        output += "while (";
        stmnt.condition->accept(*this);
        output += ")\n";
        ++indent;
        stmnt.body->accept(*this);
        --indent;
    }

    virtual void visit_statement(StatementFor& stmnt) {
        generate_indent();
        output += "for (";
        if (stmnt.stmnt1_scope->body.size() > 0) {
            assert(stmnt.stmnt1_scope->body.size() == 1);
            skip_indent = true;
            stmnt.stmnt1_scope->body.front()->accept(*this);
            skip_indent = false;
        } else {
            output += ";";
        }
        output += " ";

        if (stmnt.expr2) {
            stmnt.expr2->accept(*this);
        }
        output += "; ";

        if (stmnt.expr3) {
            stmnt.expr3->accept(*this);
        }
        output += ")\n";

        ++indent;
        stmnt.body->accept(*this);
        --indent;
    }

    virtual void visit_statement(StatementReturn& stmnt) {
        generate_indent();
        output += "return ";
        stmnt.expr->accept(*this);
        output += ";\n";
    }

    virtual void visit_statement(StatementDecl&) {
        assert(false);
    }

    virtual void visit_statement(StatementDeclVar& stmnt) {
        generate_indent();
        generate_type(stmnt.exec_type);
        output += " " + stmnt.name;

        if (stmnt.assign_expr) {
            output += " = ";
            stmnt.assign_expr->accept(*this);
        }

        output += ";";
        if (!skip_indent) {
            output += "\n";
        }
    }

    virtual void visit_statement(StatementDeclFn& stmnt) {
        if (indent == 0) {
            if (std::get<1>(stmnt.exec_type.actual_type).return_type) {
                generate_type(*std::get<1>(stmnt.exec_type.actual_type).return_type, false);
            } else {
                output += "void";
            }

            output += " " + stmnt.name + "(";
            bool first = true;
            for (const StatementDecl& arg : stmnt.arguments) {
                if (!first) {
                    output += ", ";
                }
                generate_type(arg.exec_type);
                output += " " + arg.name;
                first = false;
            }
            output += ") {\n";

            ++indent;
            if (is_stmnt_main_function(stmnt)) {
                generate_indent();
                output += "std::cout << std::boolalpha;\n\n";
            }
            for (const std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
                st->accept(*this);
            }
            --indent;
            output += "}\n";
            return;
        }

        // void (* const fn) () = [] () -> void { printf("Hello, World\n"); };
        generate_indent();
        // Function type return type
        if (std::get<1>(stmnt.exec_type.actual_type).return_type) {
            generate_type(*std::get<1>(stmnt.exec_type.actual_type).return_type, false);
        } else {
            output += "void";
        }
        output += " (* const ";
        output += stmnt.name;
        output += ") (";

        // Function type argument list
        bool first = true;
        for (const StatementDecl& arg : stmnt.arguments) {
            if (!first) {
                output += ", ";
            }
            generate_type(arg.exec_type);
            first = false;
        }

        output += ") = [] (";

        // Lambda argument list
        first = true;
        for (const StatementDecl& arg : stmnt.arguments) {
            if (!first) {
                output += ", ";
            }
            generate_type(arg.exec_type);
            output += " " + arg.name;
            first = false;
        }
        output += ") -> ";

        // Lambda return type
        if (std::get<1>(stmnt.exec_type.actual_type).return_type) {
            generate_type(*std::get<1>(stmnt.exec_type.actual_type).return_type, false);
        } else {
            output += "void";
        }

        output += " {\n";
        ++indent;
        for (const std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            st->accept(*this);
        }
        --indent;
        generate_indent();
        output += "};\n";
    }

    virtual void visit_expression(Expression&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionUnary& expr) {
        switch (expr.op) {
        case ExpressionUnary::Op::Not:
            output += "!";
            break;
        case ExpressionUnary::Op::Minus:
            output += "-";
            break;
        default:
            assert(false);
        }
        expr.expr->accept(*this);
    }

    virtual void visit_expression(ExpressionBinary& expr) {
        output += "(";
        expr.expr_l->accept(*this);
        switch (expr.op) {
        case ExpressionBinary::Op::Minus:
            output += " - ";
            break;
        case ExpressionBinary::Op::Plus:
            output += " + ";
            break;
        case ExpressionBinary::Op::Times:
            output += " * ";
            break;
        case ExpressionBinary::Op::Divide:
            output += " / ";
            break;
        case ExpressionBinary::Op::Or:
            output += " || ";
            break;
        case ExpressionBinary::Op::And:
            output += " && ";
            break;
        case ExpressionBinary::Op::Assign:
            output += " = ";
            break;
        case ExpressionBinary::Op::Equals:
            output += " == ";
            break;
        case ExpressionBinary::Op::NotEquals:
            output += " != ";
            break;
        case ExpressionBinary::Op::LessThan:
            output += " < ";
            break;
        case ExpressionBinary::Op::MoreThan:
            output += " > ";
            break;
        case ExpressionBinary::Op::LessThanOrEquals:
            output += " <= ";
            break;
        case ExpressionBinary::Op::MoreThanOrEquals:
            output += " >= ";
            break;
        default:
            assert(false);
        }
        expr.expr_r->accept(*this);
        output += ")";
    }

    virtual void visit_expression(ExpressionCall& expr) {
        expr.callable->accept(*this);
        output += "(";
        bool first = true;
        for (const std::unique_ptr<Expression>& arg : expr.arg_list) {
            if (!first) {
                output += ", ";
            }
            arg->accept(*this);
            first = false;
        }
        output += ")";
    }

    virtual void visit_expression(ExpressionGroup& expr) {
        output += "(";
        expr.expr->accept(*this);
        output += ")";
    }

    virtual void visit_expression(ExpressionLiteral&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionLiteralString& expr) {
        output += '\"';
        for (char c : expr.value) {
            switch (c) {
            case '\n':
                output += "\\n";
                break;
            case '\0':
                output += "\\0";
                break;
            case '\r':
                output += "\\r";
                break;
            default:
                output += c;
            }
        }
        output += "\"sv";
    }

    virtual void visit_expression(ExpressionLiteralNumber& expr) {
        output += std::to_string(expr.value);
    }

    virtual void visit_expression(ExpressionLiteralBool& expr) {
        output += expr.value ? "true" : "false";
    }

    virtual void visit_expression(ExpressionLiteralIdentifier& expr) {
        output += expr.name;
    }

    [[nodiscard]] static bool is_stmnt_main_function(const Statement& stmnt) {
        if (typeid(stmnt) != typeid(StatementDeclFn&)) {
            return false;
        }
        const StatementDeclFn& st = reinterpret_cast<const StatementDeclFn&>(stmnt);
        return st.name == "main" &&
            st.arguments.empty() &&
            std::get<1>(st.exec_type.actual_type).return_type &&
            std::get<1>(st.exec_type.actual_type).return_type->actual_type.index() == 0 &&
            std::get<0>(std::get<1>(st.exec_type.actual_type).return_type->actual_type) == ExecType::BasicType::Number;
    }

    void start() {
        if (const auto& it = global_scope.bindings.find("main"); it == global_scope.bindings.end() || !is_stmnt_main_function(it->second)) {
            fprintf(stderr, "Error: Program needs a \"main() -> Number\" function and doesn't have it!\n");
            throw ExitMsg(1);
        }

        output += "#include <iostream>\n";
        output += "#include <string_view>\n";
        output += "using namespace std::literals::string_view_literals;\n\n";
        for (const std::unique_ptr<Statement>& st : global_scope.body) {
            st->accept(*this);
        }
    }

public:
    static std::string generate(const Scope& global_scope) {
        CppCodegener codegener(global_scope);
        codegener.start();
        return std::move(codegener.output);
    }
};

} // namespace Compiler::NewAST
