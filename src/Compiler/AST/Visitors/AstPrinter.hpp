#pragma once
#include "Compiler/AST/Scope.hpp"


namespace Compiler::AST {

class AstPrinter : public AstVisitor {
    AstPrinter() = default;
    uint32_t indent = 0;
    bool do_indent = true;

    void print_indent() const {
        for (uint32_t i = 0; i < indent; i++) {
            putc(' ', stdout);
        }
    }

    static void print_indent(uint32_t indent) {
        for (uint32_t i = 0; i < indent; i++) {
            putc(' ', stdout);
        }
    }

    void visit_exec_type(const ExecType& exec_type) {
        switch (exec_type.actual_type.index()) {
        case std::variant_npos:
            printf("Unknown");
            break;
        case 0:
            printf("%s", exec_type.is_mutable ? "mut " : "");
            switch (std::get<0>(exec_type.actual_type)) {
            case ExecType::BasicType::String:
                printf("String");
                break;
            case ExecType::BasicType::Number:
                printf("Number");
                break;
            case ExecType::BasicType::Bool:
                printf("Bool");
                break;
            case ExecType::BasicType::Void:
                printf("void");
                break;
            default:
                assert(false);
            }
            break;
        default:
            assert(false);
        }
    }

    virtual void visit_statement(Statement&) {
        assert(false);
    }

    virtual void visit_statement(StatementEmpty&) {
        print_indent();
        printf("StatementEmpty ()\n");
    }

    virtual void visit_statement(StatementExpression& stmnt) {
        print_indent();
        printf("StatementExpression (\n");
        ++indent;
        stmnt.expr->accept(*this);
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_statement(StatementPrint& stmnt) {
        print_indent();
        printf("StatementPrint (\n");
        ++indent;
        stmnt.expr->accept(*this);
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_statement(StatementBlock& stmnt) {
        print_indent();
        printf("StatementBlock (\n");
        ++indent;
        for (std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            st->accept(*this);
        }
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_statement(StatementIfElse& expr) {
        print_indent();
        printf("StatementIfElse (\n");
        print_indent();
        printf("IF\n");
        ++indent;
        expr.condition->accept(*this);
        --indent;
        print_indent();
        printf("THEN\n");
        ++indent;
        expr.if_branch->accept(*this);
        --indent;
        if (expr.else_branch) {
            print_indent();
            printf("ELSE\n");
            expr.else_branch->accept(*this);
        }
        print_indent();
        printf(") FI\n");
    }

    virtual void visit_statement(StatementWhile& stmnt) {
        print_indent();
        printf("StatementWhile (\n");
        print_indent();
        printf("WHILE\n");
        ++indent;
        stmnt.condition->accept(*this);
        --indent;

        print_indent();
        printf("DO\n");
        ++indent;
        stmnt.body->accept(*this);
        --indent;

        print_indent();
        printf(") DONE\n");
    }

    virtual void visit_statement(StatementFor& stmnt) {
        print_indent();
        printf("StatementFor (\n");
        print_indent();
        printf("FOR\n");
        ++indent;
        for (const std::unique_ptr<Statement>& st : stmnt.stmnt1_scope->body) {
            st->accept(*this);
        }
        --indent;
        print_indent();
        printf(";\n");
        if (stmnt.expr2) {
            ++indent;
            stmnt.expr2->accept(*this);
            --indent;
        }
        print_indent();
        printf(";\n");
        if (stmnt.expr3) {
            ++indent;
            stmnt.expr3->accept(*this);
            --indent;
        }
        print_indent();
        printf("DO\n");
        ++indent;
        stmnt.body->accept(*this);
        --indent;
        print_indent();
        printf(") DONE\n");
    }

    virtual void visit_statement(StatementReturn& stmnt) {
        print_indent();
        printf("StatementReturn (\n");
        ++indent;
        stmnt.expr->accept(*this);
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_statement(StatementDecl&) {
        print_indent();
        printf("StatementDecl\n");
        assert(false);
    }

    virtual void visit_statement(StatementDeclVar& stmnt) {
        print_indent();
        printf("StatementDeclVar (");

        if (!stmnt.assign_expr) {
            printf(" ");
            visit_exec_type(stmnt.exec_type);
            printf(" %s )\n", stmnt.name.c_str());
        } else {
            printf("\n");
            print_indent(indent + 1);
            visit_exec_type(stmnt.exec_type);
            printf(" %s = ", stmnt.name.c_str());

            do_indent = false;
            stmnt.assign_expr->accept(*this);
            do_indent = true;

            print_indent(indent);
            printf(")\n");
        }
    }

    virtual void visit_statement(StatementDeclFn& stmnt) {
        print_indent();
        printf("StatementDeclFn [fn %s(", stmnt.name.c_str());
        bool first = true;
        for (const auto& arg : stmnt.arguments) {
            printf(first ? "" : ", ");
            visit_exec_type(arg.exec_type);
            printf(" %s", arg.name.c_str());
            first = false;
        }
        printf(") -> ");
        if (std::get<1>(stmnt.exec_type.actual_type).return_type) {
            visit_exec_type(*std::get<1>(stmnt.exec_type.actual_type).return_type);
        } else {
            printf("void");
        }
        printf("] (\n");
        ++indent;
        for (std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            st->accept(*this);
        }
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_expression(Expression&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionUnary& expr) {
        print_indent();
        printf("ExpressionUnary [");
        switch (expr.op) {
        case ExpressionUnary::Op::Not:
            printf("Not");
            break;
        case ExpressionUnary::Op::Minus:
            printf("Minus");
            break;
        default:
            assert(false);
        }
        printf("] (\n");
        ++indent;
        expr.expr->accept(*this);
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_expression(ExpressionBinary& expr) {
        print_indent();
        printf("ExpressionBinary [");
        switch (expr.op) {
        case ExpressionBinary::Op::Minus:
            printf("Minus");
            break;
        case ExpressionBinary::Op::Plus:
            printf("Plus");
            break;
        case ExpressionBinary::Op::Times:
            printf("Times");
            break;
        case ExpressionBinary::Op::Divide:
            printf("Divide");
            break;
        case ExpressionBinary::Op::Or:
            printf("Or");
            break;
        case ExpressionBinary::Op::And:
            printf("And");
            break;
        case ExpressionBinary::Op::Assign:
            printf("Assign");
            break;
        case ExpressionBinary::Op::Equals:
            printf("Equals");
            break;
        case ExpressionBinary::Op::NotEquals:
            printf("NotEquals");
            break;
        case ExpressionBinary::Op::LessThan:
            printf("LessThan");
            break;
        case ExpressionBinary::Op::MoreThan:
            printf("MoreThan");
            break;
        case ExpressionBinary::Op::LessThanOrEquals:
            printf("LessThanOrEquals");
            break;
        case ExpressionBinary::Op::MoreThanOrEquals:
            printf("MoreThanOrEquals");
            break;
        default:
            assert(false);
        }
        printf("] (\n");
        ++indent;
        expr.expr_l->accept(*this);
        expr.expr_r->accept(*this);
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_expression(ExpressionCall& expr) {
        print_indent();
        printf("ExpressionCall (\n");
        print_indent();
        printf("CALLE\n");
        ++indent;
        expr.callable->accept(*this);
        --indent;
        print_indent();
        printf("ARGS\n");
        ++indent;
        for (std::unique_ptr<Expression>& x : expr.arg_list) {
            x->accept(*this);
        }
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_expression(ExpressionGroup& expr) {
        print_indent();
        printf("ExpressionGroup (\n");
        ++indent;
        expr.expr->accept(*this);
        --indent;
        print_indent();
        printf(")\n");
    }

    virtual void visit_expression(ExpressionLiteral&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionLiteralString& expr) {
        if (do_indent) {
            print_indent();
        }
        printf("ExpressionLiteralString ( \"%s\" )\n", expr.value.c_str());
    }

    virtual void visit_expression(ExpressionLiteralNumber& expr) {
        if (do_indent) {
            print_indent();
        }
        printf("ExpressionLiteralNumber ( %d )\n", expr.value);
    }

    virtual void visit_expression(ExpressionLiteralBool& expr) {
        if (do_indent) {
            print_indent();
        }
        printf("ExpressionLiteralBool ( %s )\n", expr.value ? "true" : "false");
    }

    virtual void visit_expression(ExpressionLiteralIdentifier& expr) {
        if (do_indent) {
            print_indent();
        }
        printf("ExpressionLiteralIdentifier ( %s )\n", expr.name.c_str());
    }

    void print_program(const Scope& global_scope) {
        for (const auto& stmnt : global_scope.body) {
            stmnt->accept(*this);
        }
    }

public:
    static void printout(const Scope& global_scope) {
        AstPrinter printer;
        printer.print_program(global_scope);
    }
};

} // namespace Compiler::NewAST
