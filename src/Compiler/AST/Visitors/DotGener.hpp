#pragma once
#include "Compiler/AST/Scope.hpp"


namespace Compiler::AST {

/**
 * Generates a description of the AST in the 'dot' language, which can be used to generate
 * pictures of graphes in various formats with the opensource tool 'Graphviz'.
 */
class DotGener : public AstVisitor {
    DotGener() = default;
    std::string output;
    uint32_t depth = 0;

    void print_exec_type(const ExecType& exec_type) {
        if (exec_type.is_mutable) {
            output += "mut ";
        }
        switch (exec_type.actual_type.index()) {
        case std::variant_npos:
            output += "Unknown";
            break;
        case 0:
            switch (std::get<0>(exec_type.actual_type)) {
            case ExecType::BasicType::Unknown:
                output += "Unknown";
                break;
            case ExecType::BasicType::String:
                output += "String";
                break;
            case ExecType::BasicType::Number:
                output += "Number";
                break;
            case ExecType::BasicType::Bool:
                output += "Bool";
                break;
            case ExecType::BasicType::Void:
                output += "void";
                break;
            default:
                assert(false);
            }
            break;
        case 1: {
            output += "fn (";
            bool first = true;
            for (const auto& [arg_type, arg_name] : std::get<1>(exec_type.actual_type).arg_list) {
                if (!first) {
                    output += ", ";
                }
                print_exec_type(arg_type);
                first = false;
            }
            output += ") -> ";
            if (std::get<1>(exec_type.actual_type).return_type) {
                print_exec_type(*std::get<1>(exec_type.actual_type).return_type);
            } else {
                output += "void";
            }
            break;
        } case 2:
        default:
            assert(false);
        }
    }

    void print_statement_id(Statement& stmnt, uint32_t depth) {
        output += "stmnt" + std::to_string(depth) + "_" + std::to_string(stmnt.line_num) + "_" + std::to_string(stmnt.line_pos);
    }

    void print_expression_id(Expression& expr, uint32_t depth) {
        output += "expr" + std::to_string(depth) + "_" + std::to_string(expr.line_num) + "_" + std::to_string(expr.line_pos);
    }

    virtual void visit_statement(Statement&) {
        assert(false);
    }

    virtual void visit_statement(StatementEmpty& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Empty]\"] ;\n";
    }

    virtual void visit_statement(StatementExpression& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Expr]\"] ;\n";

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_expression_id(*stmnt.expr, depth + 1);
        output += " ;\n";


        ++depth;
        stmnt.expr->accept(*this);
        --depth;
    }

    virtual void visit_statement(StatementPrint& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Print]\"] ;\n";

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_expression_id(*stmnt.expr, depth + 1);
        output += " ;\n";

        ++depth;
        stmnt.expr->accept(*this);
        --depth;
    }

    virtual void visit_statement(StatementBlock& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Block]\"];\n";

        for (std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_statement_id(*st, depth + 1);
            output += " ;\n";

            ++depth;
            st->accept(*this);
            --depth;
        }
    }

    virtual void visit_statement(StatementIfElse& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt IfElse]\"];\n";

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_expression_id(*stmnt.condition, depth + 1);
        output += " [xlabel=\"condition\"];\n";

        ++depth;
        stmnt.condition->accept(*this);
        --depth;

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_statement_id(*stmnt.if_branch, depth + 1);
        output += " [xlabel=\"if\"];\n";

        ++depth;
        stmnt.if_branch->accept(*this);
        --depth;

        if (stmnt.else_branch) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_statement_id(*stmnt.else_branch, depth + 1);
            output += " [xlabel=\"else\"];\n";

            ++depth;
            stmnt.else_branch->accept(*this);
            --depth;
        }
    }

    virtual void visit_statement(StatementWhile& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt While]\"] ;\n";

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_expression_id(*stmnt.condition, depth + 1);
        output += " [xlabel=\"condition\"];\n";
        ++depth;
        stmnt.condition->accept(*this);
        --depth;

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_statement_id(*stmnt.body, depth + 1);
        output += " [xlabel=\"body\"];\n";
        ++depth;
        stmnt.body->accept(*this);
        --depth;
    }

    virtual void visit_statement(StatementFor& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt For]\"] ;\n";

        assert(stmnt.stmnt1_scope->body.size() <= 1);
        if (stmnt.stmnt1_scope->body.size() > 0) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_statement_id(*stmnt.stmnt1_scope->body.front(), depth + 1);
            output += " [xlabel=\"Stmnt 1\"];\n";

            ++depth;
            stmnt.stmnt1_scope->body.front()->accept(*this);
            --depth;
        }

        if (stmnt.expr2) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_expression_id(*stmnt.expr2, depth + 1);
            output += " [xlabel=\"Expr 2\"];\n";

            ++depth;
            stmnt.expr2->accept(*this);
            --depth;
        }

        if (stmnt.expr3) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_expression_id(*stmnt.expr3, depth + 1);
            output += " [xlabel=\"Expr 3\"];\n";

            ++depth;
            stmnt.expr3->accept(*this);
            --depth;
        }

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_statement_id(*stmnt.body, depth + 1);
        output += " [xlabel=\"body\"];\n";

        ++depth;
        stmnt.body->accept(*this);
        --depth;
    }

    virtual void visit_statement(StatementReturn& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Return]\"];\n";

        print_statement_id(stmnt, depth);
        output += " -- ";
        print_expression_id(*stmnt.expr, depth + 1);
        output += " ;\n";

        ++depth;
        stmnt.expr->accept(*this);
        --depth;
    }

    virtual void visit_statement(StatementDecl&) {
        assert(false);
    }

    virtual void visit_statement(StatementDeclVar& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Decl Var]\\n";
        print_exec_type(stmnt.exec_type);
        output += " " + stmnt.name + "\"] ;\n";

        if (stmnt.assign_expr) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_expression_id(*stmnt.assign_expr, depth + 1);
            output += " ;\n";

            ++depth;
            stmnt.assign_expr->accept(*this);
            --depth;
        }
    }

    virtual void visit_statement(StatementDeclFn& stmnt) {
        print_statement_id(stmnt, depth);
        output += " [label=\"[Stmnt Decl Fn]\\nfn " + stmnt.name + "(";
        bool first = true;
        for (const auto& arg : stmnt.arguments) {
            if (!first) {
                output += ", ";
            }
            print_exec_type(arg.exec_type);
            output += " " + arg.name;
            first = false;
        }
        output += ") -> ";
        if (std::get<1>(stmnt.exec_type.actual_type).return_type) {
            print_exec_type(*std::get<1>(stmnt.exec_type.actual_type).return_type);
        } else {
            output += "void";
        }
        output += "\"] ;\n";

        for (const std::unique_ptr<Statement>& st : stmnt.local_scope->body) {
            print_statement_id(stmnt, depth);
            output += " -- ";
            print_statement_id(*st, depth + 1);
            output += " ;\n";

            ++depth;
            st->accept(*this);
            --depth;
        }
    }


    virtual void visit_expression(Expression&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionUnary& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"Expr Unary ";
        switch (expr.op) {
        case ExpressionUnary::Op::Not:
            output += "Not";
            break;
        case ExpressionUnary::Op::Minus:
            output += "Minus";
            break;
        default:
            assert(false);
        }
        output += "\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\"];\n";

        print_expression_id(expr, depth);
        output += " -- ";
        print_expression_id(*expr.expr, depth + 1);
        output += " ;\n";
        ++depth;
        expr.expr->accept(*this);
        --depth;
    }

    virtual void visit_expression(ExpressionBinary& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Binary ";
        switch (expr.op) {
        case ExpressionBinary::Op::Minus:
            output += "Minus";
            break;
        case ExpressionBinary::Op::Plus:
            output += "Plus";
            break;
        case ExpressionBinary::Op::Times:
            output += "Times";
            break;
        case ExpressionBinary::Op::Divide:
            output += "Divide";
            break;
        case ExpressionBinary::Op::Or:
            output += "Or";
            break;
        case ExpressionBinary::Op::And:
            output += "And";
            break;
        case ExpressionBinary::Op::Assign:
            output += "Assign";
            break;
        case ExpressionBinary::Op::Equals:
            output += "Equals";
            break;
        case ExpressionBinary::Op::NotEquals:
            output += "NotEquals";
            break;
        case ExpressionBinary::Op::LessThan:
            output += "LessThan";
            break;
        case ExpressionBinary::Op::MoreThan:
            output += "MoreThan";
            break;
        case ExpressionBinary::Op::LessThanOrEquals:
            output += "LessThanOrEquals";
            break;
        case ExpressionBinary::Op::MoreThanOrEquals:
            output += "MoreThanOrEquals";
            break;
        default:
            assert(false);
            __builtin_unreachable();
        }
        output += "]\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\"];\n";

        print_expression_id(expr, depth);
        output += " -- ";
        print_expression_id(*expr.expr_l, depth + 1);
        output += " ;\n";
        ++depth;
        expr.expr_l->accept(*this);
        --depth;

        print_expression_id(expr, depth);
        output += " -- ";
        print_expression_id(*expr.expr_r, depth + 1);
        output += " ;\n";
        ++depth;
        expr.expr_r->accept(*this);
        --depth;
    }

    virtual void visit_expression(ExpressionCall& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Call]\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\"] ;\n";

        print_expression_id(expr, depth);
        output += " -- ";
        print_expression_id(*expr.callable, depth + 1);
        output += " [xlabel=\"called object\"];\n";

        ++depth;
        expr.callable->accept(*this);
        --depth;

        uint32_t i = 0;
        for (const std::unique_ptr<Expression>& arg : expr.arg_list) {
            ++i;
            print_expression_id(expr, depth);
            output += " -- ";
            print_expression_id(*arg, depth + 1);
            output += " [xlabel=\"arg ";
            output += std::to_string(i);
            output += "\"];\n";

            ++depth;
            arg->accept(*this);
            --depth;
        }
    }

    virtual void visit_expression(ExpressionGroup& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Group]\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\"];\n";

        print_expression_id(expr, depth);
        output += " -- ";
        print_expression_id(*expr.expr, depth + 1);
        output += " ;\n";
        ++depth;
        expr.expr->accept(*this);
        --depth;
    }

    virtual void visit_expression(ExpressionLiteral&) {
        assert(false);
    }

    virtual void visit_expression(ExpressionLiteralString& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Literal String]\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\\n\\\"";
        output += expr.value;
        output += "\\\"\"] ;\n";
    }

    virtual void visit_expression(ExpressionLiteralNumber& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Literal Number]\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\\n" + std::to_string(expr.value) + "\"] ;\n";
    }

    virtual void visit_expression(ExpressionLiteralBool& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Literal Bool]\\nType: ";
        print_exec_type(expr.exec_type);
        output += expr.value ? "\\ntrue" : "\\nfalse";
        output += "\"] ;\n";
    }

    virtual void visit_expression(ExpressionLiteralIdentifier& expr) {
        print_expression_id(expr, depth);
        output += " [label=\"[Expr Literal Identifier]\\nType: ";
        print_exec_type(expr.exec_type);
        output += "\\n" + expr.name + "\"] ;\n";
    }


    void generate_from_global_scope(const Scope& global_scope) {
        output += "graph \"\" {\n";
        output += "graph [label=\"Orthogonal edges\", splines=ortho, nodesep=0.8]\n";
        output += "edge [weight=2]\n";
        output += "global_scope [label=\"Global Scope\"] ;\n";
        for (const std::unique_ptr<Statement>& st: global_scope.body) {
            output += "global_scope -- ";
            print_statement_id(*st, 0);
            output += " ;\n";
            st->accept(*this);
        }
        output += "}\n";
    }

public:
    static std::string generate(const Scope& global_scope) {
        DotGener dotgener;
        dotgener.generate_from_global_scope(global_scope);
        return std::move(dotgener.output);
    }
};

} // namespace Compiler::NewAST
