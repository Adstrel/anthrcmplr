#pragma once
#include "Compiler/AST/Expression.hpp"


namespace Compiler::AST {

struct Scope;

struct Statement {
    Statement(size_t line_num, size_t line_pos)
        : line_num(line_num), line_pos(line_pos) {}

    virtual ~Statement() = default;
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    size_t line_num;
    size_t line_pos;
};

struct StatementEmpty : public Statement {
    StatementEmpty(size_t line_num, size_t line_pos)
        : Statement(line_num, line_pos) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }
};

struct StatementExpression : public Statement {
    StatementExpression(size_t line_num, size_t line_pos, std::unique_ptr<Expression>&& expr)
        : Statement(line_num, line_pos), expr(std::move(expr)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    std::unique_ptr<Expression> expr;
};

struct StatementPrint : public Statement {
    StatementPrint(size_t line_num, size_t line_pos, std::unique_ptr<Expression>&& expr)
        : Statement(line_num, line_pos), expr(std::move(expr)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    std::unique_ptr<Expression> expr;
};

struct StatementBlock : public Statement {
    StatementBlock(size_t line_num, size_t line_pos, std::unique_ptr<Scope>&& local_scope)
        : Statement(line_num, line_pos), local_scope(std::move(local_scope)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    std::unique_ptr<Scope> local_scope;
};

struct StatementIfElse : public Statement {
    StatementIfElse(size_t line_num, size_t line_pos, std::unique_ptr<Expression>&& condition, std::unique_ptr<Statement>&& if_branch, std::unique_ptr<Statement>&& else_branch)
        : Statement(line_num, line_pos), condition(std::move(condition)), if_branch(std::move(if_branch)), else_branch(std::move(else_branch)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    std::unique_ptr<Expression> condition;
    std::unique_ptr<Statement> if_branch;
    // Optional
    std::unique_ptr<Statement> else_branch;
};

struct StatementWhile : public Statement {
    StatementWhile(size_t line_num, size_t line_pos, std::unique_ptr<Expression>&& condition, std::unique_ptr<Statement>&& body)
        : Statement(line_num, line_pos), condition(std::move(condition)), body(std::move(body)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    std::unique_ptr<Expression> condition;
    std::unique_ptr<Statement> body;
};

struct StatementFor : public Statement {
    StatementFor(size_t line_num, size_t line_pos, std::unique_ptr<Scope>&& stmnt1_scope, std::unique_ptr<Expression>&& expr2,
            std::unique_ptr<Expression>&& expr3,std::unique_ptr<Statement>&& body)
        : Statement(line_num, line_pos), stmnt1_scope(std::move(stmnt1_scope)), expr2(std::move(expr2)), expr3(std::move(expr3)), body(std::move(body)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    // Contains 0 or 1 statement, can contain a variable declaration that is scoped for the whole For statement
    std::unique_ptr<Scope> stmnt1_scope;

    // Optional
    std::unique_ptr<Expression> expr2;
    // Optional
    std::unique_ptr<Expression> expr3;
    std::unique_ptr<Statement> body;
};

struct StatementReturn : public Statement {
    StatementReturn(size_t line_num, size_t line_pos, std::unique_ptr<Expression>&& expr)
        : Statement(line_num, line_pos), expr(std::move(expr)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    std::unique_ptr<Expression> expr;
};

struct StatementDecl : public Statement {
    StatementDecl(size_t line_num, size_t line_pos, ExecType exec_type, std::string name)
        : Statement(line_num, line_pos), exec_type(std::move(exec_type)), name(std::move(name)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    ExecType exec_type;
    std::string name;
};

struct StatementDeclVar : public StatementDecl {
    StatementDeclVar(size_t line_num, size_t line_pos, ExecType exec_type, std::string name, std::unique_ptr<Expression>&& assign_expr)
        : StatementDecl(line_num, line_pos, std::move(exec_type), std::move(name)), assign_expr(std::move(assign_expr)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    // Optional
    std::unique_ptr<Expression> assign_expr;
};

struct StatementDeclFn : public StatementDecl {
    StatementDeclFn(size_t line_num, size_t line_pos, ExecType exec_type, std::string name, std::vector<StatementDecl>&& arguments, std::unique_ptr<Scope>&& local_scope)
        : StatementDecl(line_num, line_pos, std::move(exec_type), std::move(name)), arguments(std::move(arguments)), local_scope(std::move(local_scope)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_statement(*this); }

    const std::vector<StatementDecl> arguments;
    std::unique_ptr<Scope> local_scope;
};

} // namespace Compiler::NewAST
