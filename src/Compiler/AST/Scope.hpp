#pragma once
#include "Compiler/AST/Statement.hpp"
#include "Common.hpp"

#include <cassert>
#include <map>


namespace Compiler::AST {

struct Scope {
    void add_statement(std::unique_ptr<Statement>&& stmnt) {
        assert(stmnt);
        body.push_back(std::move(stmnt));
        if (StatementDecl* decl = dynamic_cast<StatementDecl*>(body.back().get()); decl != nullptr) {
            add_binding(*decl);
        }
    }

    void add_binding(StatementDecl& st_decl) {
        if (const auto it = bindings.find(st_decl.name); it != bindings.end()) {
            fprintf(stderr, "Error: %s '%s', declared at line %ld, position %ld, was declared before as a %s on line %ld, position %ld.\n",
                typeid(st_decl) == typeid(StatementDeclVar) ? "Variable" : "Function",
                st_decl.name.c_str(),
                st_decl.line_num, st_decl.line_pos,
                typeid(it->second) == typeid(StatementDeclVar) ? "variable" : "function",
                it->second.line_num, it->second.line_pos
            );
            throw ExitMsg(1);
        }
        bindings.emplace(st_decl.name, st_decl);
    }

    std::vector<std::unique_ptr<Statement>> body;
    std::map<std::string, StatementDecl&> bindings;
};

} // namespace Compiler::NewAST
