#pragma once
#include "Compiler/AST/ExecType.hpp"
#include "Compiler/AST/AstVisitor.hpp"

#include <string>


namespace Compiler::AST {

struct Expression {
    Expression(size_t line_num, size_t line_pos, ExecType exec_type)
        : line_num(line_num), line_pos(line_pos), exec_type(std::move(exec_type)) {}

    virtual ~Expression() = default;
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    size_t line_num;
    size_t line_pos;
    ExecType exec_type;
};

struct ExpressionUnary : public Expression {
    enum class Op {
        Not,
        Minus
    };

    ExpressionUnary(size_t line_num, size_t line_pos, ExecType exec_type, Op op, std::unique_ptr<Expression>&& expr)
        : Expression(line_num, line_pos, std::move(exec_type)), op(op), expr(std::move(expr)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    Op op;
    std::unique_ptr<Expression> expr;
};

struct ExpressionBinary : public Expression {
    enum class Op {
        Minus, Plus,
        Times, Divide,
        Or, And,
        Assign,
        Equals, NotEquals,
        LessThan, MoreThan,
        LessThanOrEquals, MoreThanOrEquals
    };

    ExpressionBinary(size_t line_num, size_t line_pos, ExecType exec_type, Op op, std::unique_ptr<Expression>&& expr_l, std::unique_ptr<Expression>&& expr_r)
        : Expression(line_num, line_pos, std::move(exec_type)), op(op), expr_l(std::move(expr_l)), expr_r(std::move(expr_r)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    Op op;
    std::unique_ptr<Expression> expr_l;
    std::unique_ptr<Expression> expr_r;
};

struct ExpressionCall : public Expression {
    ExpressionCall(size_t line_num, size_t line_pos, ExecType exec_type, std::unique_ptr<Expression>&& callable, std::vector<std::unique_ptr<Expression>>&& arg_list)
        : Expression(line_num, line_pos, std::move(exec_type)), callable(std::move(callable)), arg_list(std::move(arg_list)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    std::unique_ptr<Expression> callable;
    std::vector<std::unique_ptr<Expression>> arg_list;
};

struct ExpressionGroup : public Expression {
    ExpressionGroup(size_t line_num, size_t line_pos, ExecType exec_type, std::unique_ptr<Expression>&& expr)
        : Expression(line_num, line_pos, std::move(exec_type)), expr(std::move(expr)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    std::unique_ptr<Expression> expr;
};

struct ExpressionLiteral : public Expression {
    ExpressionLiteral(size_t line_num, size_t line_pos, ExecType exec_type)
        : Expression(line_num, line_pos, std::move(exec_type)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }
};

struct ExpressionLiteralString : public ExpressionLiteral {
    ExpressionLiteralString(size_t line_num, size_t line_pos, ExecType exec_type, std::string value)
        : ExpressionLiteral(line_num, line_pos, std::move(exec_type)), value(std::move(value)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    std::string value;
};

struct ExpressionLiteralNumber : public ExpressionLiteral {
    ExpressionLiteralNumber(size_t line_num, size_t line_pos, ExecType exec_type, int32_t value)
        : ExpressionLiteral(line_num, line_pos, std::move(exec_type)), value(value) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    int32_t value;
};

struct ExpressionLiteralBool : public ExpressionLiteral {
    ExpressionLiteralBool(size_t line_num, size_t line_pos, ExecType exec_type, bool value)
        : ExpressionLiteral(line_num, line_pos, std::move(exec_type)), value(value) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    bool value;
};

struct ExpressionLiteralIdentifier : public ExpressionLiteral {
    ExpressionLiteralIdentifier(size_t line_num, size_t line_pos, ExecType exec_type, std::string name)
        : ExpressionLiteral(line_num, line_pos, std::move(exec_type)), name(std::move(name)) {}
    virtual void accept(AstVisitor& visitor) { visitor.visit_expression(*this); }

    std::string name;
};

} // namespace Compiler::NewAST
