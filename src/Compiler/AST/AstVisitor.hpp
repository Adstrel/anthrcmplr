#pragma once


namespace Compiler::AST {

struct Statement;
struct StatementEmpty;
struct StatementExpression;
struct StatementPrint;
struct StatementBlock;
struct StatementIfElse;
struct StatementWhile;
struct StatementFor;
struct StatementReturn;
struct StatementDecl;
struct StatementDeclVar;
struct StatementDeclFn;

struct Expression;
struct ExpressionUnary;
struct ExpressionBinary;
struct ExpressionCall;
struct ExpressionGroup;
struct ExpressionLiteral;
struct ExpressionLiteralString;
struct ExpressionLiteralNumber;
struct ExpressionLiteralBool;
struct ExpressionLiteralIdentifier;

class AstVisitor {
public:
    virtual void visit_statement(Statement&) = 0;
    virtual void visit_statement(StatementEmpty&) = 0;
    virtual void visit_statement(StatementExpression&) = 0;
    virtual void visit_statement(StatementPrint&) = 0;
    virtual void visit_statement(StatementBlock&) = 0;
    virtual void visit_statement(StatementIfElse&) = 0;
    virtual void visit_statement(StatementWhile&) = 0;
    virtual void visit_statement(StatementFor&) = 0;
    virtual void visit_statement(StatementReturn&) = 0;
    virtual void visit_statement(StatementDecl&) = 0;
    virtual void visit_statement(StatementDeclVar&) = 0;
    virtual void visit_statement(StatementDeclFn&) = 0;

    virtual void visit_expression(Expression&) = 0;
    virtual void visit_expression(ExpressionUnary&) = 0;
    virtual void visit_expression(ExpressionBinary&) = 0;
    virtual void visit_expression(ExpressionCall&) = 0;
    virtual void visit_expression(ExpressionGroup&) = 0;
    virtual void visit_expression(ExpressionLiteral&) = 0;
    virtual void visit_expression(ExpressionLiteralString&) = 0;
    virtual void visit_expression(ExpressionLiteralNumber&) = 0;
    virtual void visit_expression(ExpressionLiteralBool&) = 0;
    virtual void visit_expression(ExpressionLiteralIdentifier&) = 0;
};

} // namespace Compiler::NewAST
