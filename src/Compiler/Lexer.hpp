#pragma once
#include "Compiler/Token.hpp"

#include <vector>
#include <string>


namespace Compiler {

class Lexer {
    size_t index{0};
    size_t current_line{1};
    size_t current_pos{1};

    const std::string& source;
    std::vector<Token> tokens;

    Lexer(const std::string& source)
        : source(source) {}

    void advance_pos(size_t ammount = 1) {
        if (index + ammount >= source.length()) {
            fprintf(stderr, "Unexpected end of file at line %ld, position %ld!\n",
                current_line,
                current_pos
            );
            throw ExitMsg(1);
        }

        index += ammount;
        current_pos += ammount;
    }

    void advance_line() {
        ++index;
        current_pos = 1;
        ++current_line;
    }

    void lex_identificator() {
        const size_t begin_index = index;
        const size_t begin_pos = current_pos;

        while (std::isalnum(source[index]) || source[index] == '_') {
            advance_pos();
        }

        tokens.emplace_back(
            current_line,
            begin_pos,
            source.substr(begin_index, index - begin_index),
            Token::Type::LiteralIdentifier
        );
    }

    void lex_number_literal() {
        const size_t begin_index = index;
        const size_t begin_pos = current_pos;

        while (std::isalnum(source[index])) {
            advance_pos();
        }

        tokens.emplace_back(
            current_line,
            begin_pos,
            source.substr(begin_index, index - begin_index),
            Token::Type::LiteralNumber
        );
    }

    // Returns 'true' if did lex keyword, 'false' otherwise
    bool try_lex_keyword() {
        for (const auto& [repre, type] : Token::SORTED_KEYWORD_LIST) {
            if (source.compare(index, repre.length(), repre) == 0) {
                const bool is_alnum_keyword = std::isalpha(source[index]);
                const bool is_part_of_longer_identifier = is_alnum_keyword &&
                    index + repre.length() < source.length() &&
                    std::isalnum(source[index + repre.length()]);
                if (is_part_of_longer_identifier) {
                    continue;
                }

                tokens.emplace_back(
                    current_line,
                    current_pos,
                    source.substr(index, repre.length()),
                    type
                );
                advance_pos(repre.length());
                return true;
            }
        }

        return false;
    }

    void lex_string_literal() {
        const size_t begin_index = index;
        const size_t begin_pos = current_pos;
        advance_pos();

        bool next_one_escaped = false;
        while (next_one_escaped || source[index] != '\"') {
            if (source[index] == '\n') {
                fprintf(stderr, "Error: New line character in string literal at line %ld, position %ld!",
                    current_line,
                    current_pos
                );
                throw ExitMsg(1);
            }

            next_one_escaped = source[index] == '\\';
            advance_pos();
        }

        advance_pos();

        tokens.emplace_back(
            current_line,
            begin_pos,
            source.substr(begin_index, index - begin_index),
            Token::Type::LiteralString
        );
    }

    void start_lexing() {
        while (index < source.length()) {
            if (source[index] == '\n') {
                advance_line();
                continue;
            }

            if (std::isspace(source[index]) || source[index] == '\0') {
                advance_pos();
                continue;
            }

            if (try_lex_keyword()) {
                continue;
            }

            if (std::isalpha(source[index]) || source[index] == '_') {
                lex_identificator();
                continue;
            }

            if (std::isdigit(source[index])) {
                lex_number_literal();
                continue;
            }

            if (source[index] == '\"') {
                lex_string_literal();
                continue;
            }

            fprintf(stderr, "Error: Unknown token \"%d\" at line %ld, position %ld!\n",
                source[index],
                current_line,
                current_pos
            );
            throw ExitMsg(1);
        }

        tokens.emplace_back(
            current_line,
            current_pos,
            "",
            Token::Type::EndOfFile
        );
    }

public:
    [[nodiscard]] static std::vector<Token> lex(const std::string& sourcecode) {
        Lexer lexer(sourcecode);
        lexer.start_lexing();

        return std::move(lexer.tokens);
    }
};

} // namespace Compiler
