#pragma once
#include "Compiler/AST/Scope.hpp"
#include "Compiler/Token.hpp"

#include <vector>
#include <memory>
#include <string>
#include <cassert>
#include <stdexcept>


namespace Compiler::AST {

class Parser {
    class Error : public std::runtime_error {
    public:
        template<typename... Args_t>
        Error(Args_t&&... args)
            : std::runtime_error(formatf(std::forward<Args_t>(args)...)) {}
    };

    const std::vector<Token>& token_list;
    Scope global_scope;
    size_t index = 0;

    [[nodiscard]] const Token& current() const { return token_list[index]; }
    [[nodiscard]] bool current_is(Token::Type type) const { return current().type == type; }
    [[nodiscard]] const Token& previous() const { return token_list[index - 1]; }
    void advance() { ++index; }

    void verify(Token::Type type) const {
        if (!current_is(type)) {
            throw Error{
                "Expected token of type '%s' on line %ld, position %ld, but found '%s' instead!",
                Token::type_str(type).data(),
                current().line_num,
                current().line_pos,
                current().type_str().data()
            };
        }
    }

    void expect(Token::Type type) {
        verify(type);
        advance();
    }

    [[nodiscard]] bool advance_if_current_is(Token::Type type) {
        if (current_is(type)) {
            advance();
            return true;
        }
        return false;
    }

    Parser() = delete;
    Parser(const std::vector<Token>& token_list)
        : token_list(token_list) {}

    [[nodiscard]] ExecType parse_type() {
        bool is_mutable = advance_if_current_is(Token::Type::KeywordMut);

        if (!current().is_type()) {
            throw Error{
                "Missing type in declaration on line %ld, position %ld; found '%s' of type '%s' instead!",
                current().line_num,
                current().line_pos,
                current().text_representation.data(),
                current().type_str().data()
            };
        }

        advance();

        switch (previous().type) {
        case Token::Type::TypeNumber:
            return { ExecType::BasicType::Number, is_mutable };
        case Token::Type::TypeString:
            return { ExecType::BasicType::String, is_mutable };
        case Token::Type::TypeBool:
            return { ExecType::BasicType::Bool, is_mutable };
        default:
            assert(false);
            __builtin_unreachable();
        }
    }

    [[nodiscard]] std::unique_ptr<ExpressionLiteralNumber> parse_literal_number() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        assert(current_is(Token::Type::LiteralNumber));

        int32_t number;
        try {
            number = std::stoi(current().text_representation);
        } catch (const std::out_of_range& e) {
            throw Error{
                "Number literal '%s' on line %ld, position %ld, cannot fit into a 32 bit signed integer!",
                current().text_representation.c_str(),
                current().line_num,
                current().line_pos
            };
        }
        advance();

        return std::make_unique<ExpressionLiteralNumber>(
            line_num,
            line_pos,
            ExecType{ExecType::BasicType::Number},
            number
        );
    }

    [[nodiscard]] std::unique_ptr<ExpressionLiteralString> parse_literal_string() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        const std::string& repre = current().text_representation;

        std::vector<char> escaped_string;
        escaped_string.reserve(repre.length() - 1);

        bool next_is_escaped = false;
        for (size_t i = 1; i < repre.length() - 1; i++) {
            if (next_is_escaped) {
                switch (repre[i]) {
                case 'n':
                    escaped_string.push_back('\n');
                    break;
                case '0':
                    escaped_string.push_back('\0');
                    break;
                case 'r':
                    escaped_string.push_back('\r');
                    break;
                default:
                    escaped_string.push_back(repre[i]);
                }
                continue;
            }

            if (repre[i] == '\\') {
                next_is_escaped = true;
            } else {
                escaped_string.push_back(repre[i]);
            }
        }

        // escaped_string.push_back('\0');

        advance();

        return std::make_unique<ExpressionLiteralString>(
            line_num,
            line_pos,
            ExecType{ExecType::BasicType::String},
            std::string{ escaped_string.data(), escaped_string.size() }
        );
    }

    [[nodiscard]] std::unique_ptr<ExpressionLiteralIdentifier> parse_literal_identifier() {
        assert(current_is(Token::Type::LiteralIdentifier));
        advance();
        return std::make_unique<ExpressionLiteralIdentifier>(
            previous().line_num,
            previous().line_pos,
            ExecType{},
            previous().text_representation
        );
    }

    [[nodiscard]] std::unique_ptr<ExpressionLiteral> parse_literal() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;

        switch (current().type) {
        case Token::Type::LiteralNumber:
            return parse_literal_number();
        case Token::Type::LiteralString:
            return parse_literal_string();
        case Token::Type::LiteralTrue:
            advance();
            return std::make_unique<ExpressionLiteralBool>(
                line_num,
                line_pos,
                ExecType{ExecType::BasicType::Bool},
                true
            );
        case Token::Type::LiteralFalse:
            advance();
            return std::make_unique<ExpressionLiteralBool>(
                line_num,
                line_pos,
                ExecType{ ExecType::BasicType::Bool},
                false
            );
        case Token::Type::LiteralIdentifier:
            return parse_literal_identifier();
        default:
            assert(false);
            __builtin_unreachable();
        }
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_primary_expression() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;

        if (current().is_literal()) {
            return parse_literal();
        }

        // Grouping
        if (advance_if_current_is(Token::Type::LeftRoundBracket)) {
            std::unique_ptr<Expression> expr = parse_expression();
            expect(Token::Type::RightRoundBracket);
            return std::make_unique<ExpressionGroup>(
                line_num,
                line_pos,
                ExecType{},
                std::move(expr)
            );
        }

        throw Error{
            "Unknown token '%s' of type '%s' at line %ld, position %ld!",
            current().text_representation.c_str(),
            current().type_str().data(),
            current().line_num,
            current().line_pos
        };
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_call_expression() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        std::unique_ptr<Expression> expr = parse_primary_expression();

        if (!advance_if_current_is(Token::Type::LeftRoundBracket)) {
            return expr;
        }

        std::vector<std::unique_ptr<Expression>> arg_list;
        if (!current_is(Token::Type::RightRoundBracket)) {
            do {
                arg_list.push_back(parse_expression());
            } while (advance_if_current_is(Token::Type::Comma));
        }

        expect(Token::Type::RightRoundBracket);
        return std::make_unique<ExpressionCall>(
            line_num,
            line_pos,
            ExecType{},
            std::move(expr),
            std::move(arg_list)
        );
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_unary_expression() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        Token::Type type = current().type;
        if (advance_if_current_is(Token::Type::OperatorNot) || advance_if_current_is(Token::Type::OperatorMinus)) {
            return std::make_unique<ExpressionUnary>(
                line_num,
                line_pos,
                ExecType{},
                type == Token::Type::OperatorNot ? ExpressionUnary::Op::Not : ExpressionUnary::Op::Minus,
                parse_unary_expression()
            );
        }
        return parse_call_expression();
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_factor_expression() {
        std::unique_ptr<Expression> expr = parse_unary_expression();
        while (current_is(Token::Type::OperatorDivide) || current_is(Token::Type::OperatorTimes)) {
            const size_t line_num = current().line_num;
            const size_t line_pos = current().line_pos;
            Token::Type type = current().type;
            advance();
            expr = std::make_unique<ExpressionBinary>(
                line_num,
                line_pos,
                ExecType{},
                type == Token::Type::OperatorDivide ? ExpressionBinary::Op::Divide : ExpressionBinary::Op::Times,
                std::move(expr),
                parse_unary_expression()
            );
        }
        return expr;
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_term_expression() {
        std::unique_ptr<Expression> expr = parse_factor_expression();
        while (current_is(Token::Type::OperatorMinus) || current_is(Token::Type::OperatorPlus)) {
            const size_t line_num = current().line_num;
            const size_t line_pos = current().line_pos;
            Token::Type type = current().type;
            advance();
            expr = std::make_unique<ExpressionBinary>(
                line_num,
                line_pos,
                ExecType{},
                type == Token::Type::OperatorMinus ? ExpressionBinary::Op::Minus : ExpressionBinary::Op::Plus,
                std::move(expr),
                parse_factor_expression()
            );
        }
        return expr;
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_comparison_expression() {
        std::unique_ptr<Expression> expr = parse_term_expression();
        while (current_is(Token::Type::OperatorLessThan) || current_is(Token::Type::OperatorMoreThan) ||
                current_is(Token::Type::OperatorLessThanOrEquals) || current_is(Token::Type::OperatorMoreThanOrEquals)) {
            const size_t line_num = current().line_num;
            const size_t line_pos = current().line_pos;
            ExpressionBinary::Op op;
            switch (current().type) {
            case Token::Type::OperatorLessThan:
                op = ExpressionBinary::Op::LessThan;
                break;
            case Token::Type::OperatorMoreThan:
                op = ExpressionBinary::Op::MoreThan;
                break;
            case Token::Type::OperatorLessThanOrEquals:
                op = ExpressionBinary::Op::LessThanOrEquals;
                break;
            case Token::Type::OperatorMoreThanOrEquals:
                op = ExpressionBinary::Op::MoreThanOrEquals;
                break;
            default:
                __builtin_unreachable();
            }
            advance();
            expr = std::make_unique<ExpressionBinary>(
                line_num,
                line_pos,
                ExecType{},
                op,
                std::move(expr),
                parse_term_expression()
            );
        }
        return expr;
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_equality_expression() {
        std::unique_ptr<Expression> expr = parse_comparison_expression();
        while (current_is(Token::Type::OperatorEqualsEquals) || current_is(Token::Type::OperatorNotEquals)) {
            const size_t line_num = current().line_num;
            const size_t line_pos = current().line_pos;
            Token::Type type = current().type;
            advance();
            expr = std::make_unique<ExpressionBinary>(
                line_num,
                line_pos,
                ExecType{},
                type == Token::Type::OperatorEqualsEquals ? ExpressionBinary::Op::Equals : ExpressionBinary::Op::NotEquals,
                std::move(expr),
                parse_comparison_expression()
            );
        }
        return expr;
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_logic_expression() {
        std::unique_ptr<Expression> expr = parse_equality_expression();
        while (current_is(Token::Type::OperatorAnd) || current_is(Token::Type::OperatorOr)) {
            const size_t line_num = current().line_num;
            const size_t line_pos = current().line_pos;
            Token::Type type = current().type;
            advance();
            expr = std::make_unique<ExpressionBinary>(
                line_num,
                line_pos,
                ExecType{},
                type == Token::Type::OperatorAnd ? ExpressionBinary::Op::And : ExpressionBinary::Op::Or,
                std::move(expr),
                parse_equality_expression()
            );
        }
        return expr;
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_assignment_expression() {
        std::unique_ptr<Expression> expr = parse_logic_expression();
        while (current_is(Token::Type::OperatorEquals)) {
            const size_t line_num = current().line_num;
            const size_t line_pos = current().line_pos;
            advance();
            expr = std::make_unique<ExpressionBinary>(
                line_num,
                line_pos,
                ExecType{},
                ExpressionBinary::Op::Assign,
                std::move(expr),
                parse_logic_expression()
            );
        }
        return expr;
    }

    [[nodiscard]] std::unique_ptr<Expression> parse_expression() {
        return parse_assignment_expression();
    }

    [[nodiscard]] std::unique_ptr<StatementPrint> parse_print_statement() {
        assert(current_is(Token::Type::KeywordPrint));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;

        advance();
        expect(Token::Type::LeftRoundBracket);
        std::unique_ptr<Expression> expr = parse_expression();
        expect(Token::Type::RightRoundBracket);
        expect(Token::Type::Semicolon);

        return std::make_unique<StatementPrint>(
            line_num,
            line_pos,
            std::move(expr)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementBlock> parse_block_statement() {
        assert(current_is(Token::Type::LeftCurlyBracket));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        advance();

        std::unique_ptr<Scope> local_scope = std::make_unique<Scope>();
        while (!advance_if_current_is(Token::Type::RightCurlyBracket)) {
            local_scope->add_statement(parse_statement());
        }

        return std::make_unique<StatementBlock>(
            line_num,
            line_pos,
            std::move(local_scope)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementIfElse> parse_if_statement() {
        assert(current_is(Token::Type::KeywordIf));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        advance();

        expect(Token::Type::LeftRoundBracket);
        std::unique_ptr<Expression> condition = parse_expression();
        expect(Token::Type::RightRoundBracket);

        std::unique_ptr<Statement> if_branch = parse_statement();

        if (typeid(*if_branch) == typeid(StatementDeclVar) || typeid(*if_branch) == typeid(StatementDeclFn)) {
            throw Error{
                "A lone declaration statement after an 'if', on %ld, position %ld, needs to be in a block!",
                if_branch->line_num,
                if_branch->line_pos
            };
        }

        std::unique_ptr<Statement> else_branch;
        if (advance_if_current_is(Token::Type::KeywordElse)) {
            else_branch = parse_statement();

            if (typeid(*else_branch) == typeid(StatementDeclVar) || typeid(*else_branch) == typeid(StatementDeclFn)) {
                throw Error{
                    "A lone declaration statement after an 'else', on %ld, position %ld, needs to be in a block!",
                    else_branch->line_num,
                    else_branch->line_pos
                };
            }
        }

        return std::make_unique<StatementIfElse>(
            line_num,
            line_pos,
            std::move(condition),
            std::move(if_branch),
            std::move(else_branch)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementWhile> parse_while_statement() {
        assert(current_is(Token::Type::KeywordWhile));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        advance();

        expect(Token::Type::LeftRoundBracket);
        std::unique_ptr<Expression> condition = parse_expression();
        expect(Token::Type::RightRoundBracket);

        std::unique_ptr<Statement> stmnt = parse_statement();

        if (StatementDecl* decl = dynamic_cast<StatementDecl*>(stmnt.get()); decl != nullptr) {
            throw Error{
                "A lone declaration statement after a 'while', on %ld, position %ld, needs to be in a block!",
                decl->line_num,
                decl->line_pos
            };
        }

        return std::make_unique<StatementWhile>(
            line_num,
            line_pos,
            std::move(condition),
            std::move(stmnt)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementFor> parse_for_statement() {
        assert(current_is(Token::Type::KeywordFor));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        advance();

        expect(Token::Type::LeftRoundBracket);
        std::unique_ptr<Scope> stmnt1_scope = std::make_unique<Scope>();
        if (current_is(Token::Type::KeywordMut) || current().is_type()) {
            stmnt1_scope->add_statement(parse_variable_declaration_statement());
        } else if (!advance_if_current_is(Token::Type::Semicolon)) {
            stmnt1_scope->add_statement(parse_expression_statement());
        }

        std::unique_ptr<Expression> expr2;
        if (!current_is(Token::Type::Semicolon)) {
            expr2 = parse_expression();
        }
        expect(Token::Type::Semicolon);

        std::unique_ptr<Expression> expr3;
        if (!current_is(Token::Type::RightRoundBracket)) {
            expr3 = parse_expression();
        }
        expect(Token::Type::RightRoundBracket);

        std::unique_ptr<Statement> body = parse_statement();

        if (typeid(*body) == typeid(StatementDeclVar) || typeid(*body) == typeid(StatementDeclFn)) {
            throw Error{
                "A lone declaration statement after a 'for', on %ld, position %ld, needs to be in a block!",
                body->line_num,
                body->line_pos
            };
        }

        return std::make_unique<StatementFor>(
            line_num,
            line_pos,
            std::move(stmnt1_scope),
            std::move(expr2),
            std::move(expr3),
            std::move(body)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementReturn> parse_return_statement() {
        assert(current_is(Token::Type::KeywordReturn));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        advance();

        std::unique_ptr<Expression> expr = parse_expression();
        expect(Token::Type::Semicolon);

        return std::make_unique<StatementReturn>(
            line_num,
            line_pos,
            std::move(expr)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementExpression> parse_expression_statement() {
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        std::unique_ptr<Expression> expr = parse_expression();
        expect(Token::Type::Semicolon);
        return std::make_unique<StatementExpression>(
            line_num,
            line_pos,
            std::move(expr)
        );
    }

    [[nodiscard]] std::unique_ptr<Statement> parse_statement() {
        switch (current().type) {
        case Token::Type::KeywordPrint:
            return parse_print_statement();
        case Token::Type::Semicolon:
            advance();
            return std::make_unique<StatementEmpty>(
                previous().line_num,
                previous().line_pos
            );
        case Token::Type::LeftCurlyBracket:
            return parse_block_statement();
        case Token::Type::KeywordIf:
            return parse_if_statement();
        case Token::Type::KeywordWhile:
            return parse_while_statement();
        case Token::Type::KeywordFor:
            return parse_for_statement();
        case Token::Type::KeywordFn:
            return parse_function_declaration_statement();
        case Token::Type::KeywordReturn:
            return parse_return_statement();
        default:
            if (current_is(Token::Type::KeywordMut) || current().is_type()) {
                return parse_variable_declaration_statement();
            }

            return parse_expression_statement();
        }
    }

    [[nodiscard]] std::unique_ptr<StatementDeclVar> parse_variable_declaration_statement() {
        ExecType exec_type = parse_type();

        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        expect(Token::Type::LiteralIdentifier);
        std::string var_name = previous().text_representation;

        std::unique_ptr<Expression> expr;
        if (advance_if_current_is(Token::Type::OperatorEquals)) {
            expr = parse_expression();
        }

        expect(Token::Type::Semicolon);

        return std::make_unique<StatementDeclVar>(
            line_num,
            line_pos,
            std::move(exec_type),
            std::move(var_name),
            std::move(expr)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementDeclFn> parse_function_declaration_statement() {
        assert(current_is(Token::Type::KeywordFn));
        const size_t line_num = current().line_num;
        const size_t line_pos = current().line_pos;
        advance();

        expect(Token::Type::LiteralIdentifier);
        std::string fn_name = previous().text_representation;

        expect(Token::Type::LeftRoundBracket);
        std::vector<std::pair<ExecType, std::string>> arg_types;
        std::vector<StatementDecl> arguments;
        if (current_is(Token::Type::KeywordMut) || current().is_type()) {
            do {
                ExecType arg_type = parse_type();
                const size_t arg_line_num = current().line_num;
                const size_t arg_line_pos = current().line_pos;
                expect(Token::Type::LiteralIdentifier);
                arg_types.emplace_back(std::move(arg_type), previous().text_representation);

                arguments.emplace_back(
                    arg_line_num,
                    arg_line_pos,
                    arg_types.back().first,
                    previous().text_representation
                );
            } while (advance_if_current_is(Token::Type::Comma));
        }

        expect(Token::Type::RightRoundBracket);
        expect(Token::Type::Arrow);

        std::unique_ptr<ExecType> return_type;
        if (advance_if_current_is(Token::Type::KeywordVoid)) {
            return_type = std::make_unique<ExecType>(ExecType::BasicType::Void);
        } else {
            return_type = std::make_unique<ExecType>(parse_type());
        }

        expect(Token::Type::LeftCurlyBracket);

        std::unique_ptr<Scope> local_scope = std::make_unique<Scope>();

        // Add the function arguments to the scope's bindings
        for (StatementDecl& arg : arguments) {
            local_scope->add_binding(arg);
        }
        // DANGER: Do not let the vector 'arguments' realloc, since we now store the references of its members

        while (!advance_if_current_is(Token::Type::RightCurlyBracket)) {
            local_scope->add_statement(parse_statement());
        }

        return std::make_unique<StatementDeclFn>(
            line_num,
            line_pos,
            ExecType{
                ExecType::FunctionType{
                    std::move(return_type),
                    std::move(arg_types),
                    fn_name
                }
            },
            std::move(fn_name),
            std::move(arguments),
            std::move(local_scope)
        );
    }

    [[nodiscard]] std::unique_ptr<StatementDecl> parse_declaration_statement() {
        if (current_is(Token::Type::KeywordFn)) {
            return parse_function_declaration_statement();
        }
        if (current_is(Token::Type::KeywordMut) || current().is_type()) {
            return parse_variable_declaration_statement();
        }

        throw Error{
            "Unknown token '%s' of type '%s' at line %ld, position %ld!",
            current().text_representation.c_str(),
            current().type_str().data(),
            current().line_num,
            current().line_pos
        };
    }

    void parse_program() {
        while (!current_is(Token::Type::EndOfFile)) {
            global_scope.add_statement(parse_declaration_statement());
        }
    }
public:

    static Scope parse(const std::vector<Token>& token_list) {
        Parser parser(token_list);

        try {
            parser.parse_program();
        } catch (const Error& e) {
            fprintf(stderr, "Error: %s\n", e.what());
            throw ExitMsg(1);
        }

        return std::move(parser.global_scope);
    }
};

} // namespace Compiler::AST
