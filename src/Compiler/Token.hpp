#pragma once
#include "Common.hpp"
#include "Magic.hpp"

#include <array>
#include <string_view>
#include <string>
#include <algorithm>


namespace Compiler {

#define DEFINE_ENUM_ELEMENT(element_name, element_string) element_name,
#define DEFINE_ENUM_REPRESENTATION(element_name, element_string) std::string_view(STRINGIFY(element_name)),
#define DEFINE_ENUM_STRING_VALUE(element_name, element_string) std::pair(std::string_view(element_string), Type::element_name),

#define ENUMERATE_TOKEN_TYPES(...)                          \
    public:                                                 \
        enum class Type {                                   \
            MAP2(DEFINE_ENUM_ELEMENT, __VA_ARGS__)          \
        };                                                  \
    private:                                                \
        static constexpr std::array TOKEN_TO_TEXT {         \
            MAP2(DEFINE_ENUM_REPRESENTATION, __VA_ARGS__)   \
        };                                                  \
        static constexpr std::array UNFILTERED_TOKEN_LIST { \
            MAP2(DEFINE_ENUM_STRING_VALUE, __VA_ARGS__)     \
        }

consteval size_t get_constexpr_filter_result_size(auto token_array, auto filter_func) {
    const auto [first, last] = std::ranges::remove_if(token_array, filter_func);
    return token_array.size() - std::distance(first, last);
}

struct Token {
    ENUMERATE_TOKEN_TYPES(
        // Literals
        LITERAL_FIRST, "",
        LiteralIdentifier, "",
        LiteralNumber, "",
        LiteralString, "",
        LiteralTrue, "true",
        LiteralFalse, "false",
        LITERAL_LAST, "",

        // Keywords
        KEYWORD_FIRST, "",
        KeywordIf, "if",
        KeywordElse, "else",
        KeywordFor, "for",
        KeywordWhile, "while",
        KeywordPrint, "print",
        KeywordMut, "mut",
        KeywordFn, "fn",
        KeywordVoid, "void",
        KeywordReturn, "return",
        KEYWORD_LAST, "",

        // Types
        TYPE_FIRST, "",
        TypeNumber, "Number",
        TypeString, "String",
        TypeBool, "Bool",
        TYPE_LAST, "",

        // Operators
        OPERATOR_FIRST, "",
        OperatorEquals, "=",
        OperatorNot, "!",
        OperatorEqualsEquals, "==",
        OperatorNotEquals, "!=",
        OperatorLessThan, "<",
        OperatorMoreThan, ">",
        OperatorLessThanOrEquals, "<=",
        OperatorMoreThanOrEquals, ">=",
        OperatorPlus, "+",
        OperatorMinus, "-",
        OperatorTimes, "*",
        OperatorDivide, "/",
        OperatorAnd, "&&",
        OperatorOr, "||",
        OPERATOR_LAST, "",

        // Other
        SingleLineComment, "",
        MultiLineComment, "",
        Whitespace, "",
        NewLine, "",
        LeftRoundBracket, "(",
        RightRoundBracket, ")",
        LeftCurlyBracket, "{",
        RightCurlyBracket, "}",
        Arrow, "->",
        Comma, ",",
        Semicolon, ";",

        EndOfFile, ""
    );

public:
    static constexpr std::array SORTED_KEYWORD_LIST = [] (auto token_array) {
        std::array<
            std::pair<std::string_view, Type>,
            get_constexpr_filter_result_size(
                UNFILTERED_TOKEN_LIST,
                [] (const auto& pair) { return pair.first.empty(); }
            )
        > result;

        std::copy_if(
            token_array.begin(), token_array.end(), result.begin(),
            [] (const auto& pair) { return !pair.first.empty(); }
        );

        std::ranges::sort(
            result,
            [] (const auto& l, const auto& r) {
                if (l.first.length() == r.first.length()) {
                    return l.first < r.first;
                }
                return l.first.length() > r.first.length();
            }
        );

        return result;
    } (UNFILTERED_TOKEN_LIST);

    static std::string_view type_str(Type type) {
        return TOKEN_TO_TEXT[static_cast<uint32_t>(type)];
    }

    std::string_view type_str() const {
        return type_str(type);
    }

    static bool is_literal(Type type) {
        uint32_t repre = static_cast<uint32_t>(type);
        uint32_t min   = static_cast<uint32_t>(Type::LITERAL_FIRST);
        uint32_t max   = static_cast<uint32_t>(Type::LITERAL_LAST);
        return repre > min && repre < max;
    }

    bool is_literal() const {
        return is_literal(type);
    }

    static bool is_keyword(Type type) {
        uint32_t repre = static_cast<uint32_t>(type);
        uint32_t min   = static_cast<uint32_t>(Type::KEYWORD_FIRST);
        uint32_t max   = static_cast<uint32_t>(Type::KEYWORD_LAST);
        return repre > min && repre < max;
    }

    bool is_keyword() const {
        return is_keyword(type);
    }

    static bool is_type(Type type) {
        uint32_t repre = static_cast<uint32_t>(type);
        uint32_t min   = static_cast<uint32_t>(Type::TYPE_FIRST);
        uint32_t max   = static_cast<uint32_t>(Type::TYPE_LAST);
        return repre > min && repre < max;
    }

    bool is_type() const {
        return is_type(type);
    }

    static bool is_operator(Type type) {
        uint32_t repre = static_cast<uint32_t>(type);
        uint32_t min   = static_cast<uint32_t>(Type::OPERATOR_FIRST);
        uint32_t max   = static_cast<uint32_t>(Type::OPERATOR_LAST);
        return repre > min && repre < max;
    }

    bool is_operator() const {
        return is_operator(type);
    }

public:
    size_t line_num;
    size_t line_pos;
    std::string text_representation;

    Type type;
};

#undef ENUMERATE_TOKEN_TYPES
#undef DEFINE_ENUM_STRING_VALUE
#undef DEFINE_ENUM_REPRESENTATION
#undef DEFINE_ENUM_ELEMENT

} // namespace Compiler
