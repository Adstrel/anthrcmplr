#pragma once
#include "Compiler/Token.hpp"
#include <cstdio>


namespace Compiler::TokenListPrinter {

void printout(const std::vector<Token>& token_list) {
    for (const Token& t : token_list) {
        if (t.text_representation.length() > 0) {
            printf("[Token - %s] Line: %ld, Pos: %ld, Text: \"%s\"\n",
                t.type_str().data(),
                t.line_num,
                t.line_pos,
                t.text_representation.c_str()
            );
        } else {
            printf("[Token - %s] Line: %ld, Pos: %ld\n",
                t.type_str().data(),
                t.line_num,
                t.line_pos
            );
        }
    }
    printf("\n");
}

} // namespace Compiler::TokenListPrinter
