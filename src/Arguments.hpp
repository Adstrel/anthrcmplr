#pragma once
#include <string_view>
#include <string>
#include <vector>

#include "Common.hpp"


struct Arguments {
    std::string output_file = "out.cpp";
    std::vector<std::string> input_files;

    [[nodiscard]] static Arguments parse(int argc, char* argv[]) {
        using namespace std::literals::string_view_literals;
        Arguments args;

        for (int i = 1; i < argc; i++) {
            if ("-h"sv == argv[i] || "--help"sv == argv[i]) {
                fprintf(stderr, "USAGE: ./program INPUT -o OUTPUT\n");
                throw ExitMsg(0);
            } else if ("-o"sv == argv[i]) {
                if (i + 1 >= argc) {
                    fprintf(stderr, "Error: Missing argument after '-o'!\n");
                    throw ExitMsg(1);
                }
                args.output_file = argv[++i];
                continue;
            } else if (argv[i][0] == '-') {
                fprintf(stderr, "Error: Unrecognized argument '%s'!\n", argv[i]);
                throw ExitMsg(1);
            } else {
                args.input_files.emplace_back(argv[i]);
                continue;
            }
        }

        if (args.input_files.empty()) {
            fprintf(stderr, "Error: No input files!\n");
            throw ExitMsg(1);
        }

        return args;
    }
};
