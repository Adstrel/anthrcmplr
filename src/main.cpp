#include "Arguments.hpp"
#include "Compiler.hpp"


int main(int argc, char* argv[]) {
    try {
        Arguments args = Arguments::parse(argc, argv);

        std::string cpp_source;
        for (const std::string& input_file : args.input_files) {
            std::string xx_source = read_from_file(input_file);
            cpp_source += Compiler::convert_to_cpp(xx_source);
        }

        printf("[Transpiled cpp code BEGIN]\n%s[Transpiled cpp code END]\n\n", cpp_source.c_str());

        write_to_file(args.output_file, cpp_source);
    } catch (const ExitMsg& m) {
        return m.exit_code;
    }

    return 0;
}
