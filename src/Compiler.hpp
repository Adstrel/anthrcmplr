#pragma once
#include "Compiler/Lexer.hpp"
#include "Compiler/TokenListPrinter.hpp"
#include "Compiler/Parser.hpp"
#include "Compiler/AST/Visitors/AstPrinter.hpp"
#include "Compiler/AST/Visitors/DotGener.hpp"
#include "Compiler/AST/Visitors/CppCodegener.hpp"
#include "Compiler/AST/Visitors/TypeResolver.hpp"
#include "Compiler/AST/Visitors/IRBuilder.hpp"


namespace Compiler {

std::string convert_to_cpp(const std::string& sourcecode) {
    std::vector<Token> token_list = Lexer::lex(sourcecode);

    // TokenListPrinter::printout(token_list);

    AST::Scope global_scope = AST::Parser::parse(token_list);
    AST::TypeResolver::resolve_types(global_scope);
    // AST::AstPrinter::printout(global_scope);

    std::string dot_source = AST::DotGener::generate(global_scope);
    // printf("[DOT SOURCE START]\n%s\n[DOT SOURCE END]\n", dot_source.c_str());
    write_to_file("out.dot", dot_source);

    std::string cpp_source = AST::CppCodegener::generate(global_scope);
    return cpp_source;
}

} // namespace Compiler
