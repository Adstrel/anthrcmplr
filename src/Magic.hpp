#pragma once


// Stringify a macro value
// https://stackoverflow.com/questions/2653214/stringification-of-a-macro-value
#define __STRINGIFY(x) #x
#define STRINGIFY(x) __STRINGIFY(x)

// Recursive forech macro
// https://stackoverflow.com/questions/6707148/foreach-macro-on-macros-arguments
// https://github.com/swansontec/map-macro/blob/ce95c50f32b3c9fed2f9780948010dccbe2b75a3/map.h
#define __EVAL_0(...) __VA_ARGS__
#define __EVAL_1(...) __EVAL_0(__EVAL_0(__EVAL_0(__VA_ARGS__)))
#define __EVAL_2(...) __EVAL_1(__EVAL_1(__EVAL_1(__VA_ARGS__)))
#define __EVAL_3(...) __EVAL_2(__EVAL_2(__EVAL_2(__VA_ARGS__)))
#define __EVAL_4(...) __EVAL_3(__EVAL_3(__EVAL_3(__VA_ARGS__)))
#define __EVAL(...)   __EVAL_4(__EVAL_4(__EVAL_4(__VA_ARGS__)))

#define __MAP_END(...)
#define __MAP_OUT
#define __MAP_COMMA ,

#define __MAP_GET_END2() 0, __MAP_END
#define __MAP_GET_END1(...) __MAP_GET_END2
#define __MAP_GET_END(...)  __MAP_GET_END1
#define __MAP_NEXT0(test, next, ...) next __MAP_OUT
#define __MAP_NEXT1(test, next) __MAP_NEXT0(test, next, 0)
#define __MAP_NEXT(test, next)  __MAP_NEXT1(__MAP_GET_END test, next)

#define __MAP_0(f, x, peek, ...) f(x) __MAP_NEXT(peek, __MAP_1)(f, peek, __VA_ARGS__)
#define __MAP_1(f, x, peek, ...) f(x) __MAP_NEXT(peek, __MAP_0)(f, peek, __VA_ARGS__)

#define __MAP2_0(f, x, y, peek, ...) f(x, y) __MAP_NEXT(peek, __MAP2_1)(f, peek, __VA_ARGS__)
#define __MAP2_1(f, x, y, peek, ...) f(x, y) __MAP_NEXT(peek, __MAP2_0)(f, peek, __VA_ARGS__)

#define __MAP_LIST_NEXT1(test, next) __MAP_NEXT0(test, __MAP_COMMA next, 0)
#define __MAP_LIST_NEXT(test, next)  __MAP_LIST_NEXT1(__MAP_GET_END test, next)

#define __MAP_LIST0(f, x, peek, ...) f(x) __MAP_LIST_NEXT(peek, __MAP_LIST_1)(f, peek, __VA_ARGS__)
#define __MAP_LIST1(f, x, peek, ...) f(x) __MAP_LIST_NEXT(peek, __MAP_LIST_0)(f, peek, __VA_ARGS__)

/**
 * Applies the function macro `f` to each of the remaining parameters.
 */
#define MAP(f, ...)  __EVAL(__MAP_1(f, __VA_ARGS__, ()()(), ()()(), ()()(), 0))
#define MAP2(f, ...) __EVAL(__MAP2_1(f, __VA_ARGS__, ()()(), ()()(), ()()(), 0))

/**
 * Applies the function macro `f` to each of the remaining parameters and
 * inserts commas between the results.
 */
#define MAP_LIST(f, ...) __EVAL(__MAP_LIST1(f, __VA_ARGS__, ()()(), ()()(), ()()(), 0))
